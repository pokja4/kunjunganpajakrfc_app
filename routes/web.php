<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('home');
Route::get('/app', array('middleware' => 'cors', 'uses' => 'HomeController@app'));
Route::get('/refreshCaptcha', array('middleware' => 'cors', 'uses' => 'HomeController@refreshCaptcha'));
Route::get('/refreshCaptcha2', array('middleware' => 'cors', 'uses' => 'HomeController@refreshCaptcha2'));
Route::post('/captchaValidate', array('middleware' => 'cors', 'uses' => 'HomeController@captchaValidate'));
Route::post('/captchaValidateIsi', array('middleware' => 'cors', 'uses' => 'HomeController@captchaValidateIsi'));
Route::post('/captchaValidate2', array('middleware' => 'cors', 'uses' => 'HomeController@captchaValidate2'));
Route::post('/saveData',  array('middleware' => 'cors', 'uses' => 'HomeController@saveData'));
Route::post('/clearCache',  array('middleware' => 'cors', 'uses' => 'HomeController@clearCache'));
Route::post('/checkCache',  array('middleware' => 'cors', 'uses' => 'HomeController@checkCache'));
Route::get('/fetch', array('middleware' => 'cors', 'uses' => 'UnitController@fetch'));

Route::post('/getNpwp', array('middleware' => 'cors', 'uses' => 'HomeController@getNpwp'));
Route::post('/getKantor', array('middleware' => 'cors', 'uses' => 'HomeController@getKantor'));
Route::post('/updateAlamatKantor', array('middleware' => 'cors', 'uses' => 'HomeController@updateAlamatKantor'));
Route::post('/getIdentitas', array('middleware' => 'cors', 'uses' => 'HomeController@getIdentitas'));
Route::post('/getLayanan', array('middleware' => 'cors', 'uses' => 'HomeController@getLayanan'));
Route::post('/getWaktuLayanan', array('middleware' => 'cors', 'uses' => 'HomeController@getWaktuLayanan'));
Route::post('/cekKodeKppAdm', array('middleware' => 'cors', 'uses' => 'HomeController@cekKodeKppAdm'));
Route::get('/searchTiket', array('middleware' => 'cors', 'uses' => 'HomeController@searchTiket'));
Route::get('/searchTiketNik/{nik}/{tipe}', array('middleware' => 'cors', 'uses' => 'HomeController@searchTiketNik'));
Route::post('/getTiket', array('middleware' => 'cors', 'uses' => 'HomeController@getTiket'));
Route::post('/cekIdentitasExist', array('middleware' => 'cors', 'uses' => 'HomeController@cekIdentitasExist'));
Route::post('/cekQuotaExist', array('middleware' => 'cors', 'uses' => 'HomeController@cekQuotaExist'));
Route::post('/cekQuotaTimeExist', array('middleware' => 'cors', 'uses' => 'HomeController@cekQuotaTimeExist'));
Route::post('/getDays', array('middleware' => 'cors', 'uses' => 'HomeController@getDays'));
Route::post('/getHoliday', array('middleware' => 'cors', 'uses' => 'HomeController@getHoliday'));
Route::post('/getKuotaDatepicker', array('middleware' => 'cors', 'uses' => 'HomeController@getKuotaDatepicker'));
Route::post('/checkAntrianPerWaktu', array('middleware' => 'cors', 'uses' => 'HomeController@checkAntrianPerWaktu'));

Route::post('/cekAntrian', array('middleware' => 'cors', 'uses' => 'HomeController@cekAntrian'));
Route::post('/cekBooking', array('middleware' => 'cors', 'uses' => 'HomeController@cekBooking'));
Route::post('/saveTiket', array('middleware' => 'cors', 'uses' => 'TiketController@saveTiket'));
Route::post('/bookingAntrian', array('middleware' => 'cors', 'uses' => 'HomeController@bookingAntrian'));
Route::get('/home-cache', function() {$exitCode = Artisan::call('cache:clear');
    return redirect()->to('/app');
});
Route::get('/tiket', array('middleware' => 'cors', 'uses' => 'HomeController@tiket'))->name('tiket');
Route::get('/mail', array('middleware' => 'cors', 'uses' => 'HomeController@mail'))->name('mail');

Route::get('/qr-code', function () {
    return QrCode::size(100)->generate('pajak.go.id');
});

?>
