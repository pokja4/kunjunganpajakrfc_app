<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:o="urn:schemas-microsoft-com:office:office">

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1" name="viewport">
    <meta name="x-apple-disable-message-reformatting">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="telephone=no" name="format-detection">
    <title></title>
    <!--[if (mso 16)]>
    <style type="text/css">
    a {text-decoration: none;}
    </style>
    <![endif]-->
    <!--[if gte mso 9]><style>sup { font-size: 100% !important; }</style><![endif]-->
    <!--[if gte mso 9]>
<xml>
    <o:OfficeDocumentSettings>
    <o:AllowPNG></o:AllowPNG>
    <o:PixelsPerInch>96</o:PixelsPerInch>
    </o:OfficeDocumentSettings>
</xml>
<![endif]-->
    <!--[if !mso]><!-- -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,400i,700,700i" rel="stylesheet">
    <!--<![endif]-->
</head>

<body>
    <div class="es-wrapper-color">
        <!--[if gte mso 9]>
			<v:background xmlns:v="urn:schemas-microsoft-com:vml" fill="t">
				<v:fill type="tile" color="#ffffff"></v:fill>
			</v:background>
		<![endif]-->
        <table class="es-wrapper" style="background-position: center top;" width="100%" cellspacing="0" cellpadding="0">
            <tbody>
                <tr>
                    <td class="esd-email-paddings" valign="top">
                        <table class="es-header esd-header-popover" cellspacing="0" cellpadding="0" align="center">
                            <tbody>
                                <tr>
                                    <td class="esd-stripe" esd-custom-block-id="15610" align="center" bgcolor="#02275d" style="background-color: #02275d;">
                                        <table class="es-header-body" style="background-color: #02275d;" width="800" cellspacing="0" cellpadding="0" align="center" bgcolor="#02275d">
                                            <tbody>
                                                <tr>
                                                    <td class="esd-structure" align="left">
                                                        <table width="100%" cellspacing="0" cellpadding="0">
                                                            <tbody>
                                                                <tr>
                                                                    <td class="esd-container-frame" width="800" valign="top" align="center">
                                                                        <table width="100%" cellspacing="0" cellpadding="0">
                                                                            <tbody>
                                                                                <tr>
                                                                                    <td align="left" class="esd-block-text">
                                                                                        <p style="color: #ffffff; line-height: 100%; padding-left: 20px"><strong><span style="font-size: 8px; line-height: 100%;"></span><br>Informasi Kunjungan Pajak Anda.<br><span style="font-size: 9px; line-height: 100%;"></span></strong><br></p>
                                                                                    </td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <table class="es-content" cellspacing="0" cellpadding="0" align="center">
                            <tbody>
                                <tr>
                                    <td class="esd-stripe" align="center">
                                        <table class="es-content-body" style="background-color: transparent; " width="800" cellspacing="0" cellpadding="0" align="center">
                                            <tbody>
                                                <tr>
                                                    <td class="esd-structure" align="left">
                                                        <table width="100%" cellspacing="0" cellpadding="0">
                                                            <tbody>
                                                                <tr>
                                                                    <td class="esd-container-frame" width="800" valign="top" align="center">
                                                                        <table style="border-radius: 3px; border-collapse: separate; background-color: #ededed;" width="100%" cellspacing="0" cellpadding="0" bgcolor="#fcfcfc">
                                                                            <tbody>
                                                                                <tr>
                                                                                    <td class="esd-block-text es-m-txt-l es-p30t es-p20r es-p20l" align="left">
                                                                                        <h2 style="color: #333333; padding-left: 20px;">Halo!</h2>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="esd-block-text es-p10t es-p20r es-p20l" bgcolor="#ededed" align="left">
                                                                                        <p style="padding-left: 20px">Sdr/i. {{ucfirst(strtoupper($nama))}} yang terhormat, <br>Terima kasih telah mendaftar pada laman kunjung pajak.</p>
                                                                                    </td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="esd-structure es-p30t es-p20r es-p20l" style="background-color: #ededed; padding: 30px;" esd-custom-block-id="15791" bgcolor="#fcfcfc" align="left">
                                                        <table width="100%" cellspacing="0" cellpadding="0">
                                                            <tbody>
                                                                <tr>
                                                                    <td class="esd-container-frame" width="760" valign="top" align="center">
                                                                        <table style="border-color: #ededed; border-style: solid; border-width: 1px; border-radius: 3px; border-collapse: separate; background-color: #ffffff;" width="100%" cellspacing="0" cellpadding="0" bgcolor="#ffffff">
                                                                            <tbody>
                                                                                <tr>
                                                                                    <td class="esd-block-text es-p20t es-p15b" align="center">
                                                                                        <h3 style="color: #333333; font-size: 16px;">Informasi Kunjungan Anda:</h3>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td class="esd-block-text es-m-txt-c" align="center">
                                                                                        <p style="color: #000000; font-size: 12px; line-height: 150%;">NOMOR TIKET : {{$nomortiket}}</p>
                                                                                        <p style="color: #000000; font-size: 12px; line-height: 150%;">NAMA: {{ucwords($nama)}}<br>NPWP : @if(!empty($npwp)) {{$npwp}} @else - @endif <br>NAMA KANTOR: {{ucfirst(strtoupper($kpp))}}<br>LAYANAN: {{ucfirst(strtoupper($layanan))}}<br>TANGGAL: {{$tanggal}}<br>NOMOR ANTRIAN: {{$antrian}}<br>WAKTU ANTRIAN: {{$waktu}}<br><br></p>
                                                                                    </td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <table class="es-content" cellspacing="0" cellpadding="0" align="center">
                            <tbody>
                                <tr>
                                    <td class="esd-stripe" align="center">
                                        <table class="es-content-body" style="background-color: #ededed;" width="800" cellspacing="0" cellpadding="0" bgcolor="#fcfcfc" align="center">
                                            <tbody>
                                                <tr>
                                                    <td class="esd-structure es-p20t es-p20r es-p20l" align="left">
                                                        <table cellpadding="0" cellspacing="0" width="100%">
                                                            <tbody>
                                                                <tr>
                                                                    <td width="760" class="esd-container-frame" align="center" valign="top">
                                                                        <table cellpadding="0" cellspacing="0" width="100%">
                                                                            <tbody>
                                                                                <tr>
                                                                                    <td align="center" class="esd-block-text">
                                                                                        <p style="line-height: 120%;"><br style="font-size: 12px;">Salam <br>Direktorat Jenderal Pajak<br>--------------------------------------------------------------<br>Email ini dikirim secara otomatis oleh sistem. Anda tidak perlu membalas atau mengirim email ke alamat ini.<br><br><br></p>
                                                                                    </td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <table cellpadding="0" cellspacing="0" class="es-footer esd-footer-popover" align="center">
                            <tbody>
                                <tr>
                                    <td class="esd-stripe" style="background-color: #02275d;" esd-custom-block-id="15625" bgcolor="#02275d" align="center">
                                        <table class="es-footer-body" style="background-color: #02275d;" width="800" cellspacing="0" cellpadding="0" bgcolor="#02275d" align="center">
                                            <tbody>
                                                <tr>
                                                    <td class="esd-structure es-p20t es-p20b es-p20r es-p20l" align="left">
                                                        <table width="100%" cellspacing="0" cellpadding="0">
                                                            <tbody>
                                                                <tr>
                                                                    <td class="esd-container-frame" width="760" valign="top" align="center">
                                                                        <table width="100%" cellspacing="0" cellpadding="0">
                                                                            <tbody>
                                                                                <tr>
                                                                                    <td esdev-links-color="#999999" class="esd-block-text es-p5b" align="center">
                                                                                        <p style="color: #ffffff; line-height: 120%;">© 2020 Direktorat Jenderal Pajak | www.pajak.go.id | djponline.pajak.go.id | Layanan Informasi dan Keluhan Kring Pajak (021) 1 500 200</p>
                                                                                    </td>
                                                                                </tr>
                                                                                <br>
                                                                            </tbody>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</body>

</html>

{{-- <div class="text-center">
    <img src="data:image/png;base64, {!! base64_encode(QrCode::format('png')->size(100)->generate('helloworld')) !!} ">
    <p>Scan me to return to the original page.</p>
</div> --}}