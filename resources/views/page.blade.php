
@section('content')
        <div class="top-bar-djp" style="margin-top: 0; position: fixed;width: 100%;z-index: 1000;">
            <button id="btn_top_menu" class="btn-djp" style="font-size: 13px; margin-top: 5px; overflow: auto; font-weight:bold"  href="#" onclick="homeURL()">Selamat Datang di Laman Kunjung Pajak</button>
        </div>

        <!-- <div id="kt_header_mobile" class="kt-header-mobile  kt-header-mobile--fixed ">
        Selamat Datang di Kunjungan Pajak...
        </div> -->

        <!-- end:: Header Mobile -->
        <div class="kt-grid kt-grid--hor kt-grid--root">
            <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver kt-page">
                <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_wrapper">
                    <div>
                        
        <!-- begin:: Header -->
        <div id="kt_header" class="kt-header kt-grid__item  kt-header--fixed " data-ktheader-minimize="on" style="position: fixed;width: 100%;z-index: 999;">
            <div class="kt-header__bottom header-pajak-top">
                <div class="kt-container">
                    <!-- begin: Header Menu -->
                    <div class="kt-header-menu-wrapper" id="kt_header_menu_wrapper_second">
                        <div id="kt_header_menu_second" class="kt-header-menu kt-header-menu-mobile ">
                            <ul class="kt-menu__nav ">
                                <li class="kt-menu__item  kt-menu__item--submenu kt-menu__item--rel" aria-haspopup="true">

                                <a style="pointer-events: auto; color: #fff;padding: 10px 13px; margin: 5px -10 -4px;border-radius:4px;padding-bottom:0px;min-height: 45px;" href="#" onclick="homeURL()">Selamat Datang di Laman Kunjung Pajak</a>
                                    <!-- <a href="javascript:;" class="kt-menu__link kt-menu__toggle">
                                        <span class="kt-menu__link-text header-menu-top">Informasi Kunjungan Aplikasi</span>
                                    </a> -->
                                </li>
                            </ul>
                        </div>
                        <div class="kt-header-toolbar">
                        </div>
                    </div>
                    <!-- end: Header Menu -->
                </div>
            </div>
        </div>
        <!-- end:: Header -->
    
                    </div>
                    <div id="body-konten" class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver kt-grid--stretch" style="margin-top: -30px;">
                        <style>
                            #body-konten{background: url('{{ asset('assets/media/bg/gedung_djp.jpg')}}');
                                background-size: cover; background-position: center;
                            }
                        </style>
                        <div class="kt-container kt-body  kt-grid kt-grid--ver" id="kt_body" style="height: 100%; overflow: auto; display:block">
                            <div class="grid-djp kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" >
                                
                             <!-- CONTENT -->
                             <!-- begin:: Content -->
                                @yield('section') {{-- Semua file section kita akan ada di bagian ini --}}
                                <!-- end:: Content -->

                            </div>
                        </div>
                    </div>
                    <div>
                        
        <!-- begin:: Footer -->
        <div class="kt-footer  kt-footer--extended  kt-grid__item" id="kt_footer" style="background-color: #212c5f; color: #FFFFFF">
            <div class="kt-footer__bottom" style="background-color: #FCEC27; color: #1B1B1B;padding: 18px 0; height: 70px">
                <div class="kt-container">
                    <div class="row text-center">
                        <div class="col-md-12">
                            <h2 style="margin-bottom: 0; letter-spacing: -1px;">
                                <strong>
                                    Pajak Kita, Untuk Kita
                                </strong>
                            </h2>
                        </div>
                    </div>
                </div>
            </div>
            <div class="kt-footer__top" style="padding:3px 0 10px 0;">
                <div class="kt-container">
                    <div class="kt-footer__wrapper">
                        <div class="kt-footer__copyright" style="color: #ccc; margin-bottom: 16px; font-size: .9em; font-weight: 500; margin-top: 30px">
                            Copyright&nbsp;&copy;&nbsp; Direktorat Jenderal Pajak.
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- end:: Footer -->
    
                    </div>
                </div>
            </div>
        </div>
        <div id="kt_scrolltop" class="kt-scrolltop">
			<i class="flaticon2-arrow-up"></i>
		</div>

@section('section')