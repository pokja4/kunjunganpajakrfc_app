@extends('page')
@extends('base')
@section('section')
<!-- begin:: Content -->
<script src="https://www.google.com/recaptcha/api.js" async defer></script>  

<div class="kt-content kt-grid__item kt-grid__item--fluid" style="padding: 20px; background: #EBECF1; margin-bottom: 40px; border-radius: 0 8px 8px 8px; box-shadow: 0 2px 4px 0 rgba(0, 0, 0, .2), 0 3px 10px 0 rgba(0, 0, 0, .19);">
    <div id="flash-message"></div>
        <div class="kt-portlet">
            <div class="kt-portlet__body kt-portlet__body--fit">
                <div class="kt-grid kt-wizard-v3 kt-wizard-v3--white" id="kt_wizard_v3" data-ktwizard-state="step-first">
                    <div class="kt-grid__item">
                        <!--begin: Form Wizard Nav -->
                        <div class="kt-wizard-v3__nav">
                            <div class="kt-wizard-v3__nav-items"  style="justify-content: center; align-items: center;">
                                <a class="kt-wizard-v3__nav-item" data-ktwizard-type="step" data-ktwizard-state="current">
                                    <div class="kt-wizard-v3__nav-body">
                                        <div class="kt-wizard-v3__nav-label">
                                            <span>1</span> Data Diri
                                        </div>
                                        <div class="kt-wizard-v3__nav-bar" style="pointer-events: none"></div>
                                    </div>
                                </a>
                                <a class="kt-wizard-v3__nav-item" data-ktwizard-type="step">
                                    <div class="kt-wizard-v3__nav-body">
                                        <div class="kt-wizard-v3__nav-label">
                                            <span>2</span> SAK
                                        </div>
                                        <div class="kt-wizard-v3__nav-bar" style="pointer-events: none"></div>
                                    </div>
                                </a>
                                <a class="kt-wizard-v3__nav-item" data-ktwizard-type="step">
                                    <div class="kt-wizard-v3__nav-body">
                                        <div class="kt-wizard-v3__nav-label">
                                            <span>3</span> Unit Vertikal
                                        </div>
                                        <div class="kt-wizard-v3__nav-bar" style="pointer-events: none"></div>
                                    </div>
                                </a>
                                <a class="kt-wizard-v3__nav-item" data-ktwizard-type="step">
                                    <div class="kt-wizard-v3__nav-body">
                                        <div class="kt-wizard-v3__nav-label">
                                            <span>4</span> Cek Antrian
                                        </div>
                                        <div class="kt-wizard-v3__nav-bar" style="pointer-events: none"></div>
                                    </div>
                                </a>
                            </div>
                        </div>
                         <!--end: Form Wizard Nav -->
                    </div>
    
                    <!--begin: Modal -->
                    <div class="modal fade" id="kt_modal_1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Antrian Expired</h5>
                                    <button type="button" class="close" data-dismiss="modal" onclick="timeout()" aria-label="Close"> 
                                    </button>
                                </div>
                                <div class="modal-body">
                                   <p>Maaf session untuk nomor antrian yang anda pilih telah habis.</p>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary"  onclick="timeout()" data-dismiss="modal">Close</button>
                                </div> 
                            </div>
                        </div>
                    </div>

                    <div class="modal fade" id="kt_modal_nik_doesnt_exist" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Kunjungan tidak diperkenankan!</h5>
                                    <button type="button" class="close" data-dismiss="modal" onclick="timeout()" aria-label="Close">
                                    </button>
                                </div>
                                <div class="modal-body">
                                   <p>Anda sudah melakukan booking atas nik tersebut.</p>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary"  onclick="timeout()" data-dismiss="modal">Close</button>
                                </div>
                            </div>
                        </div>
                    </div>
    
                    <div class="modal fade" id="kt_modal_2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Mohon Maaf</h5>
                                    <button type="button" class="close" data-dismiss="modal" onclick="timeout()" aria-label="Close">
                                    </button>
                                </div>
                                <div class="modal-body">
                                   <p>Saat ini anda tidak diperkenankan datang ke Unit Kerja yang dipilih. Terimakasih.</p>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary"  onclick="timeout()" data-dismiss="modal">Close</button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="modal fade" id="kt_modal_3" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Antrian Sudah Penuh!</h5>
                                    <button type="button" class="close" data-dismiss="modal" onclick="timeout()" aria-label="Close">
                                    </button>
                                </div>
                                <div class="modal-body">
                                   <p>Harap memilih tanggal lain.</p>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary"  onclick="timeout()" data-dismiss="modal">Close</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--end: Modal -->
                    
                    <div class="kt-grid__item kt-grid__item--fluid kt-wizard-v3__wrapper" style="margin-top: -30px">
                        <!--begin: Form Wizard Form-->
                        <form class="kt-form" id="kt_form" style="margin-top: -30px">
                            <!--begin: Form Wizard Step 1-->
                             <div class="kt-wizard-v3__content border identitaswp" style="padding: 20px" data-ktwizard-type="step-content" data-ktwizard-state="current" >
                                <div class="kt-heading kt-heading--md" style="text-align: center">Informasi Wajib Pajak</div>
                                <div class="kt-form__section kt-form__section--first">
                                    <div class="kt-wizard-v3__form">
                                        <div class="form-group row">
                                            <label class="col-xl-3 col-lg-3 col-form-label">NIK</label>
                                            <div id="spinner" class="col-lg-9 col-xl-9 kt-spinner--sm kt-spinner--success kt-spinner--right kt-spinner--input">
                                                <input class="form-control" id="input_nik" name="nik" type="text" value="" required>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-xl-3 col-lg-3 col-form-label">Nama</label>
                                            <div class="col-lg-9 col-xl-9">
                                                <input class="form-control" id="input_nama" name="nama" type="text" value="">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-xl-3 col-lg-3 col-form-label">NPWP</label>
                                            <div id="spinner2" class="col-lg-9 col-xl-9 kt-spinner--sm kt-spinner--success kt-spinner--right kt-spinner--input">
                                                <input class="form-control" id="input_npwp" name="npwp" type="text" value="">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-xl-3 col-lg-3 col-form-label">Nama NPWP</label>
                                            <div class="col-lg-9 col-xl-9 ">
                                                <input class="form-control" id="input_namanpwp" name="namanpwp" type="text" value="" readonly="">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-xl-3 col-lg-3 col-form-label">Perihal</label>
                                            <div class="col-lg-9 col-xl-9">
                                                <textarea class="form-control" id="input_perihal" name="perihal" type="text" value="" required></textarea>
                                            </div>
                                        </div>
                                          <div class="form-group row">
                                            <label class="col-xl-3 col-lg-3 col-form-label">Layanan</label>
                                            <div class="col-lg-9 col-xl-9">
                                                <select name="layanan" id="input_layanan" class="form-control" required="">
                                                    <option value="">-- Pilih Layanan --</option>
                                                        @foreach ($layanan as $key => $value)
                                                            <option value="{{ $value->id }}" required> 
                                                                {{ $value->layanan }}
                                                            </option>
                                                        @endforeach 
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label  class="col-xl-3 col-lg-3 col-form-label" for="captcha"></label>
                                            <div class="col-lg-9 col-xl-9">
                                                {!! app('captcha')->display() !!}
												<input type="text" name="captcha" id="captcha_hidden" value="" required style="opacity: 0;width: 0; float: left;" />
                                                <span class="text-danger">{{ $errors->first('g-recaptcha-response') }}</span>
                                            </div>
                                        </div> 
                                    </div>
                                </div>
                            </div>
    
                            <!--end: Form Wizard Step 1-->
    
                            <!--begin: Form Wizard Step 2-->
                            <div class="kt-wizard-v3__content border sak" data-ktwizard-type="step-content" style="padding: 20px">
                                <div class="kt-heading kt-heading--md" style="text-align: center">Pengisian SAK Masyarakat</div>
                                <div class="kt-form__section kt-form__section--first">
                                    <div class="kt-wizard-v3__form" style="margin-top: -20px">
                                        <label>Demi kesehatan dan keselamatan bersama, Anda harus JUJUR dalam menjawab pertanyaan dibawah ini.</label>
                                        <label>Dalam 14 hari terakhir, apakah Anda pernah mengalami hal hal berikut:</label>
                                        <div class="form-group" style="margin-top: 10px">
                                            <label>Apakah pernah keluar rumah/tempat umum (pasar, fasyankes, kerumunan orang, dan lain-lain)?</label>
                                            <div class="kt-radio-list">
                                                <label class="kt-radio kt-radio--success" name="radio5">
                                                    <input type="radio" class="q1_1" id="q1" name="q1" value="1" required> Ya
                                                    <span></span>
                                                </label>
                                                <label class="kt-radio kt-radio--brand" name="radio5">
                                                    <input type="radio" class="q1_0" id="q1" name="q1" value="0"> Tidak
                                                    <span></span>
                                                </label>
                                            </div>
                                        </div>
    
                                        <div class="form-group">
                                            <label>Apakah pernah menggunakan transportasi umum?</label>
                                            <div class="kt-radio-list">
                                                <label class="kt-radio kt-radio--success" name="radio5">
                                                    <input type="radio" class="q2_1" id="q2" name="q2" value="1" required> Ya
                                                    <span></span>
                                                </label>
                                                <label class="kt-radio kt-radio--brand" name="radio5">
                                                    <input type="radio" class="q2_0" id="q2" name="q2" value="0"> Tidak
                                                    <span></span>
                                                </label>
                                            </div>
                                        </div>
    
                                        <div class="form-group">
                                            <label>Apakah pernah melakukan perjalanan ke luar kota/internasional? (wilayah yang terjangkit/zona merah)</label>
                                            <div class="kt-radio-list">
                                                <label class="kt-radio kt-radio--success" name="radio5">
                                                    <input type="radio" class="q3_1" id="q3" name="q3" value="1" required> Ya
                                                    <span></span>
                                                </label>
                                                <label class="kt-radio kt-radio--brand" name="radio5">
                                                    <input type="radio" class="q3_0" id="q3" name="q3" value="0"> Tidak
                                                    <span></span>
                                                </label>
                                            </div>
                                        </div>
    
                                        <div class="form-group">
                                            <label>Apakah anda mengikuti kegiatan yang melibatkan orang banyak?</label>
                                            <div class="kt-radio-list">
                                                <label class="kt-radio kt-radio--success" name="radio5">
                                                    <input type="radio" class="q4_1" id="q4" name="q4" value="1" required> Ya
                                                    <span></span>
                                                </label>
                                                <label class="kt-radio kt-radio--brand" name="radio5">
                                                    <input type="radio" class="q4_0" id="q4" name="q4" value="0"> Tidak
                                                    <span></span>
                                                </label>
                                            </div>
                                        </div>
    
                                        <div class="form-group">
                                            <label>Apakah memiliki riwayat kontak erat dengan orang yang dinyatakan ODP, PDP, atau positif COVID-19 (berjabat tangan, berbicara, berada dalam satu ruangan/ satu rumah)?</label>
                                            <div class="kt-radio-list">
                                                <label class="kt-radio kt-radio--success" name="radio5">
                                                    <input type="radio" class="q5_1" id="q5" name="q5" value="5" required> Ya
                                                    <span></span>
                                                </label>
                                                <label class="kt-radio kt-radio--brand" name="radio5">
                                                    <input type="radio" class="q5_0" id="q6" name="q5" value="0"> Tidak
                                                    <span></span>
                                                </label>
                                            </div>
                                        </div>
    
                                        <div class="form-group">
                                            <label>Apakah pernah mengalami demam/batuk/pilek/sakit tenggorokan/sesak dalam 14 hari terakhir?</label>
                                            <div class="kt-radio-list">
                                                <label class="kt-radio kt-radio--success" name="radio5">
                                                    <input type="radio" class="q6_1" id="q6" name="q6" value="5" required > Ya
                                                    <span></span>
                                                </label>
                                                <label class="kt-radio kt-radio--brand" name="radio5">
                                                    <input type="radio" class="q6_0" id="q7" name="q6" value="0"> Tidak
                                                    <span></span>
                                                </label>
                                            </div>
                                        </div>
    
                                        
                                       
                                    </div>
                                </div>
                            </div>
    
                            <!--end: Form Wizard Step 2-->
    
                            <!--begin: Form Wizard Step 3-->
                            <div class="kt-wizard-v3__content border antrian" data-ktwizard-type="step-content" style="padding: 20px">
                                <div class="kt-heading kt-heading--md" style="text-align: center">Informasi Unit Kerja</div>
                                <div class="kt-form__section kt-form__section--first">
                                    <div class="kt-wizard-v3__form">
                                        <div class="form-group row">
                                            <label class="col-xl-3 col-lg-3 col-form-label">Nama Unit Kerja</label>
                                            <div class="col-lg-9 col-xl-9">
                                                <input type="text" name="kpp" id="kpp" class="form-control input-lg" hidden />
                                                <input type="text" name="kdkpp" id="kdkpp" class="form-control input-lg" hidden />
                                                <input type="text" name="unit_kerja" id="unit_kerja" class="form-control input-lg" placeholder="Nama Unit Kerja" />
                                                <div id="list_unit_kerja">
                                                </div>
                                            </div>
                                        </div>
                                       
                                        <div class="form-group row">
                                            <label class="col-form-label col-lg-3 col-sm-12">Tanggal Kunjungan</label>
                                            <div class="col-lg-4 col-md-9 col-sm-12">
                                                <div class="input-group date">
                                                    <input type="text" class="form-control" disabled placeholder="Pilih Tanggal" id="kt_datepicker_2" name="date" required />
                                                    <div class="input-group-append">
                                                        <span class="input-group-text">
                                                            <i class="la la-calendar-check-o"></i>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                      
                                    </div>
                                </div>
                            </div>
    
                            <!--end: Form Wizard Step 3-->
    
                            <!--begin: Form Wizard Step 4-->
                            <div class="kt-wizard-v3__content border tiket" data-ktwizard-type="step-content" style="padding: 20px">
                                <div class="kt-heading kt-heading--md" style="text-align: center">Informasi Antrian</div>
                                <div class="kt-form__section kt-form__section--first">
                                    <div class="kt-wizard-v3__form">
                                        <div class="form-group row">
                                            <label class="col-xl-4 col-lg-4 col-form-label">Nama Unit Kerja </label>
                                            <div class="col-lg-8 col-xl-8">
                                                <input class="form-control" id="p_unit_kerja" name="p_unit_kerja" type="text" value="" disabled>
                                            </div>
                                        </div>
                                        <div class="form-group row" style="margin-top: -20px;">
                                            <label class="col-xl-4 col-lg-4 col-form-label">Tanggal Kunjungan </label>
                                            <div class="col-lg-8 col-xl-8">
                                                <input class="form-control" id="p_tanggal_kunjungan" name="p_tanggal_kunjungan" type="text" value="" disabled>
                                            </div>
                                        </div>
                                        <div class="form-group row" style="margin-top: -20px;">
                                            <label class="col-xl-4 col-lg-4 col-form-label">Booking </label>
                                            <div class="col-lg-8 col-xl-8">
                                                <input class="form-control" id="p_booking" name="p_booking" type="text" value="" disabled>
                                            </div>
                                        </div>
                                        <div class="form-group row" style="margin-top: -20px;">
                                            <label class="col-xl-4 col-lg-4 col-form-label">Antrian Ke </label>
                                            <div class="col-lg-8 col-xl-8">
                                                <input class="form-control" id="p_antrian" name="p_antrian" type="text" value="" disabled>
                                            </div>
                                        </div>
                                        <div class="kt-section">
                                            <span class="kt-section__info" style="text-align: center">
                                                Informasi
                                            </span>
                                            <div class="kt-section__content kt-section__content--solid">
                                                <div id="disclaimer" class="kt-section__content">
                                                    <p id="disclaimerText1"></p>
                                                    <p id="disclaimerText2"></p>
                                                    <p id="disclaimerText3"></p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
    
                            <!--end: Form Wizard Step 4-->
    
                            <!--begin: Form Actions -->
                            <div class="kt-form__actions">
                                <div class="btn btn-secondary btn-md btn-tall btn-wide kt-font-bold kt-font-transform-u sebelum" data-ktwizard-type="action-prev">
                                    Sebelumnya
                                </div>
                                <div class="btn btn-success btn-md btn-tall btn-wide kt-font-bold kt-font-transform-u booking" data-ktwizard-type="action-submit" onclick="booking()">
                                    Booking
                                </div>
                                <div class="btn btn-brand btn-md btn-tall btn-wide kt-font-bold kt-font-transform-u berikut" id="berikutnya" data-ktwizard-type="action-next"  onclick="checkInput()">
                                    Berikutnya
                                </div>
                            </div>
                            <!--end: Form Actions -->
                        </form>
    
                        <!--end: Form Wizard Form-->
                    </div>
                </div>
            </div>
        </div>
    </div>

<!-- end:: Content -->
@endsection
<script>
</script>