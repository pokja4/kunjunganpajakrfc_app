@extends('page')
@extends('base')
@section('section')

<div class="kt-content kt-grid__item kt-grid__item--fluid" style="padding: 20px; display:grid;  margin-top: 80px; background: #EBECF1; margin-bottom: 40px; border-radius: 0 8px 8px 8px; box-shadow: 0 2px 4px 0 rgba(0, 0, 0, .2), 0 3px 10px 0 rgba(0, 0, 0, .19);">
                                    <!--Begin::Isi Konten-->
        <input type="text" value="c1a506a3-4d79-447a-93e5-f1bdb3974680" name="ac" id="ac" class="form-control" style="display: none"/>
                                    
            <div class="kt-portlet">
                <div class="kt-portlet__head" style="background: #212c5f;">
                    <div class="kt-portlet__head-label">
                        <span class="kt-portlet__head-icon" style="color: white;">
                            <i class="flaticon2-checking"></i>
                        </span>
                        <h3 class="kt-portlet__head-title" style="color: white;">Alur Kunjungan Pajak</h3>
                    </div>
                    <div class="kt-portlet__head-label">
                    </div>
                </div>
                <div class="kt-portlet__body kt-portlet--height-fluid">
                    <div style="width: 100%; text-align: center; margin-bottom: 2rem;">
                        <img style="width:100%;height: auto;" src="{{ $image}}">
                    </div>

                </div>
            </div>

            <div class="kt-portlet">
                <div class="kt-portlet__head" style="background: #212c5f;">
                    <div class="kt-portlet__head-label">
                        <span class="kt-portlet__head-icon" style="color: white;">
                            <i class="flaticon-eye"></i>
                        </span>
                        <h3 class="kt-portlet__head-title" style="color: #fff;">Disclaimer Informasi</h3>
                    </div>
                    <br/>
                </div>
                <div class="kt-portlet__body" style="background: #fff;">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="kt-section" style="margin: 0 0 0;">
                                <div class="kt-section__body">
                                    <div class="kt-portlet__head" style="border-bottom:0px">
                                        <div class="kt-portlet__head-label">
                                            <h3 class="kt-portlet__head-title" style="color:#02275d">
                                                {{ $info->keterangan}}
                                            </h3>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col">
                    <div class="kt-section" id="kt_wizard_v3" data-ktwizard-state="step-first">
                            <button type="button" onclick="window.location.href='{{ url('app')}}'" class="btn btn-info btn-pill btn-elevate kt-login__btn-primary" style="color: #fff; font-weight: bold;"><i class="flaticon2-add"></i> DAFTAR</button>
                            <button type="button" onclick="window.location.href='{{ url('searchTiket')}}'" class="btn btn-warning btn-pill btn-elevate kt-login__btn-primary" style="color: #02275d; font-weight: bold;"><i class="flaticon2-search"></i> CARI TIKET</button>
                            <button target="_blank" type="button" onClick="window.open('https://www.pajak.go.id/index.php/id/unit-kerja')" class="btn btn-success btn-pill btn-elevate kt-login__btn-primary" style="color: #fff; font-weight: bold;"><i class="flaticon-eye"></i> Klik disini untuk melihat unit kerja</button>&nbsp;
                    </div>
                </div>
            </div>

            <div class="kt-portlet">   
                
            </div>
        
                                    <!--End::Isi Konten-->
</div>

@endsection
<script>

</script>