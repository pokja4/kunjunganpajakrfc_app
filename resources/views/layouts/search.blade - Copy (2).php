@extends('page')
@extends('base')
@section('section')
<!-- begin:: Content -->

<div class="kt-content kt-grid__item kt-grid__item--fluid col-md-8" style="padding: 20px; margin-top: 80px; background: #EBECF1; margin-bottom: 40px; border-radius: 0 8px 8px 8px; box-shadow: 0 2px 4px 0 rgba(0, 0, 0, .2), 0 3px 10px 0 rgba(0, 0, 0, .19);">
<div id="notfound"></div>
    <div class="row" style="position: relative">
        <div class="halaman-utama col-md-12">
                <div class="kt-portlet kt-portlet--height-fluid kt-portlet--head-sm ">
                    <div class="kt-portlet__head" style="background: #212c5f;">
                        <div class="kt-portlet__head-label">
                            <span class="kt-portlet__head-icon" style="color: white">
                                <i class="flaticon2-search"></i>
                            </span>
                            <h3 class="kt-portlet__head-title" style="color: white">
                                Pencarian Tiket</h3>
                        </div>
                    </div>
                <div class="kt-portlet__body">

                        <div class="kt-grid kt-wizard-v2 kt-wizard-v2--white" id="kt_wizard_v2" data-ktwizard-state="step-first">
                            
                            <div class="kt-grid__item" style="margin-top: 10px">
                                <!--begin: Form Wizard Form-->
                                <form class="kt-form-search" id="kt_form" style="margin-top: 10px">
                                    <!--begin: Form Wizard Step 1-->
                                    <div class="kt-wizard-v3__content identitaswp" style="padding: 20px" data-ktwizard-type="step-content" data-ktwizard-state="current" >
                                        <div class="kt-form__section kt-form__section--first">
                                            <div class="kt-wizard-v3__form">
                                                 <div class="form-group ">
                                                    <div class="row">
                                                        <div class="col-md-2"></div>
                                                        <p class="col-form-label col-md-9" for="nik">Anda dapat melakukan pencarian tiket berdasarkan NIK dan/atau Nomor Tiket</p>
                                                    </div>
                                                    <div class="row" style="margin-bottom: 10px;">
                                                        <div class="col-md-2"></div>
                                                        <div class="col-md-1">
                                                            <div class="kt-checkbox-inline">
                                                                <span class="form-text text-muted"></span>
                                                                <label class="kt-checkbox kt-checkbox--bold kt-checkbox--brand">
                                                                    <input type="checkbox" value="cbNIK" name="cbNik" id="cbNik" checked="true" autocomplete="off">
                                                                        NIK <span></span>
                                                                </label>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-1"></div>
                                                        <div class="col-md-1">
                                                            <div class="kt-checkbox-inline">
                                                                <span class="form-text text-muted"></span>
                                                                <label class="kt-checkbox kt-checkbox--bold kt-checkbox--brand">
                                                                    <input type="checkbox" value="cbPaspor" name="cbPaspor" id="cbPaspor" autocomplete="off">
                                                                        Paspor <span></span>
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row divPaspor" hidden>
                                                        <div class="col-md-2"></div>
                                                        <label class="col-form-label col-md-2" for="nik">Paspor</label>
                                                        <div class="col-md-6">
                                                            <div class="kt-input-icon kt-input-icon--right password-djp">
                                                                <input class="form-control" id="cari_paspor" name="cari_paspor" type="text" value="" autocomplete="off" d placeholder="Masukkan Paspor Anda">
                                                            </div>
                                                            <div id="efin-error" class="help-block help-block-error invalid-feedback" style="display: block;"></div>
                                                        </div>
                                                    </div>
                                                    <div class="row divNik">
                                                        <div class="col-md-2"></div>
                                                        <label class="col-form-label col-md-2" for="nik">NIK</label>
                                                        <div class="col-md-6">
                                                            <div class="kt-input-icon kt-input-icon--right password-djp">
                                                                <input class="form-control" id="cari_nik" name="cari_nik" type="text" value="" autocomplete="off" d placeholder="Masukkan NIK Anda">
                                                            </div>
                                                            <div id="efin-error" class="help-block help-block-error invalid-feedback" style="display: block;"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group ">
                                                    <div class="row">
                                                        <div class="col-md-2"></div>
                                                        <label class="col-form-label col-md-2" for="nik">Nomor Tiket</label>
                                                        <div class="col-4 col-sm-3 col-md-2 col-lg-2 col-xl-2">
                                                            <input class="form-control carinomortiket" id="cari_nomor_tiket1" name="nomor_tiket1" type="text" value="">
                                                        </div>
                                                        <label class="col-form-label">-</label>
                                                        <div class="col-4 col-sm-3 col-md-2 col-lg-2 col-xl-2">
                                                            <input class="form-control carinomortiket" id="cari_nomor_tiket2" name="nomor_tiket2" type="text" value="">
                                                        </div>
                                                        <label class="col-form-label">-</label>
                                                        <div class="col-4 col-sm-3 col-md-2 col-lg-2 col-xl-2">
                                                            <input class="form-control carinomortiket" id="cari_nomor_tiket3" name="nomor_tiket3" type="text" value="">
                                                        </div>
                                                    </div>
                                                </div> 
                                                <!-- <div class="form-group row captcha3" >
                                                    <label  class="col-xl-4 col-lg-4 col-form-label" for="captcha"></label>
                                                    <div class="col-lg-6">
                                                        <div class="captchaa3">
                                                            <span>{!! captcha_img() !!}</span>
                                                            <button type="button" class="btn btn-brand btn-djp btn-elevate btn-pill kt-login__btn-primary"><i class="la la-refresh" id="refresh"></i></button>
                                                        </div> 
                                                        <div class="input-group">
                                                            <input class="form-control" id="captcha3" name="captcha3" placeholder="Masukkan captcha" type="text" autocomplete="off">
                                                        </div>
                                                        <input class="form-control" id="captcha_isi3" name="captcha_isi3" style="opacity: 0;width: 0; float: left;" autocomplete="off" >
                                                    </div>
                                                </div>   -->                                           
                                                <div class="form-group row">
                                                    <label class="col-xl-2 col-lg-2 col-form-label"></label>
                                                    <div class="col-lg-7 col-xl-7">
                                                        <button type="submit" id="caritikett" class="btn btn-warning btn-pill btn-elevate kt-login__btn-primary" style="color: #02275d; font-weight: bold;"><i class="flaticon2-search"></i> Cari TIKET</button>&nbsp;
                                                        <button onclick="timeoutHome()" class="btn btn-djp btn-elevate btn-pill" style="color: #fff; font-weight: bold;"><i class="flaticon-refresh"></i> Batal</button>&nbsp;
                                                    </div>
                                                </div>


                                            </div>
                                        </div>
                                    </div>
                                    <!--end: Form Wizard Step 1-->

                                </form>
                                <!--end: Form Wizard Form-->
                                    
                            </div>
                        </div>


                            <!--begin: Modal -->
                            <div class="modal fade " id="kt_modal_caritiket" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" style="max-width: 100%, top: 25%">
                                <div class="modal-dialog" role="document" style="max-width:94%;">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLabel">Hasil Pencarian Berdasarkan NIK/Paspor</h5>
                                        </div>
                                        <div class="modal-body">
                                            <div class="kt-grid kt-wizard-v3 kt-wizard-v3--white caritiket" id="kt_wizard_v5" data-ktwizard-state="step-first" style="visibility: visible;">
                                                <div class="kt-grid__item">
                                                    <div class="kt-grid__item kt-grid__item--fluid kt-wizard-v3__wrapper">
                                                        <div class="kt-wizard-v3__content col-md-12" style="padding: 5px">
                                                            <!--begin: Datatable -->
                                                            <div class="table-responsive">
                                                                <table class="table table-striped table-bordered table-hover table-checkable tiket_table" id="tiket_table" width="100%">
                                                                    <thead class="thead-light" style="background-color:#02275d">
                                                                    <tr style="background-color:#02275d">
                                                                        <th style="color: #646c9a; font-size: 14;">No</th>
                                                                        <th style="color: #646c9a; font-size: 14;">NIK/Paspor</th>
                                                                        <th style="color: #646c9a; font-size: 14;">Layanan</th>
                                                                        <th style="color: #646c9a; font-size: 14;">No Tiket</th>
                                                                        <th style="color: #646c9a; font-size: 14;">Tanggal Kunjungan</th>
                                                                        <th style="color: #646c9a; font-size: 14;">Jam Kunjungan</th>
                                                                        <th style="color: #646c9a; font-size: 14;">Aksi</th>
                                                                    </tr>
                                                                    </thead>
                                                                    <tbody style="font-size: 14;">
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                            <!--end: Datatable -->
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--end: Form Wizard Step 1-->
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--end: Modal -->
                </div>
            </div>
        </div>
    </div>
</div>

<!-- end:: Content -->
@endsection
<script>
</script>
