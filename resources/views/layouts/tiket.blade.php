@extends('page')
@extends('base')
@section('section')

<div class="kt-content kt-grid__item kt-grid__item--fluid" style="padding: 20px; display:grid;  margin-top: 80px; background: #EBECF1; margin-bottom: 40px; border-radius: 0 8px 8px 8px; box-shadow: 0 2px 4px 0 rgba(0, 0, 0, .2), 0 3px 10px 0 rgba(0, 0, 0, .19);">
    <div class="row" style="position: relative">
        <div class="halaman-utama col-md-12">
                <div class="kt-portlet kt-portlet--height-fluid kt-portlet--head-sm ">
                    <div class="kt-portlet__head" style="background: #212c5f;">
                        <div class="kt-portlet__head-label">
                            <span class="kt-portlet__head-icon" style="color: white">
                                <i class="flaticon2-document"></i>
                            </span>
                            <h3 class="kt-portlet__head-title" style="color: white">
                                Tiket Antrean</h3>
                        </div>
                    </div>
                        <div class="kt-portlet__body kt-portlet__body--fit">
                            <div class="kt-grid kt-wizard-v3 kt-wizard-v3--white" id="kt_wizard_v3" data-ktwizard-state="step-first">
                                <div class="kt-grid__item">
                                    <div class="kt-grid__item kt-grid__item--fluid kt-wizard-v3__wrapper" style="margin: 20px">
                                        <div class="kt-wizard-v3__content border col-md-8" style="padding: 20px; margin-bottom: 10px">
                                        <div class="kt-section">
                                            <div class="kt-section__content kt-section__content--solid"  style="border-left: 4px solid #fd397a; padding: 10px;">
                                                <div id="disclaimer" class="kt-section__content" style="text-align: center">
                                                    <p>{!! nl2br(e($info->tkt_notifikasi)) !!}</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="kt-heading kt-heading--md" style="text-align: center; font-weight: bold">Nomor Tiket {{$antrian_data->tiket}}
                                        <br>
                                        <br>
                                        {!! QrCode::generate($url); !!}
                                        </div>
                                        <div class="kt-form__section kt-form__section--first">
                                            <div class="kt-wizard-v3__form">
                                                <div class="form-group row">
                                                    <label class="col-xl-4 col-lg-4 col-form-label">Nama</label>
                                                    <div class="col-lg-8 col-xl-8">
                                                        <input class="form-control" style="border-color:#32a852; color: #32a852" type="text" value="{{ ucfirst(trans($antrian_data->nama))}}" disabled>
                                                    </div>
                                                </div>
                                                <div class="form-group row" style="margin-top: -20px;">
                                                    <label class="col-xl-4 col-lg-4 col-form-label">Identitas (NIK / Paspor) </label>
                                                    <div class="col-lg-8 col-xl-8">
                                                        <input class="form-control" style="border-color:#32a852; color: #32a852" type="text" value="{{$antrian_data->identitas}}" disabled>
                                                    </div>
                                                </div>
                                            <div class="form-group row" style="margin-top: -20px;">
                                                    <label class="col-xl-4 col-lg-4 col-form-label">NPWP </label>
                                                    <div class="col-lg-8 col-xl-8">
                                                        <input class="form-control" style="border-color:#32a852; color: #32a852" type="text" value="{{$antrian_data->npwp}}" disabled>
                                                    </div>
                                                </div>
                                                <div class="form-group row" style="margin-top: -20px;">
                                                    <label class="col-xl-4 col-lg-4 col-form-label">Nama Kantor</label>
                                                    <div class="col-lg-8 col-xl-8">
                                                        <input class="form-control" style="border-color:#32a852; color: #32a852" type="text" value="{{$antrian_data->kpp}}" disabled>
                                                    </div>
                                                </div>
                                                <div class="form-group row" style="margin-top: -20px;">
                                                    <label class="col-xl-4 col-lg-4 col-form-label">Alamat Kantor</label>
                                                    <div class="col-lg-8 col-xl-8">
                                                        <textarea class="form-control" style="border-color:#32a852; color: #32a852" type="text" disabled>{{$estimasi->ALAMAT}}, {{$estimasi->KOTA}}</textarea>
                                                    </div>
                                                </div>
                                                <div class="form-group row" style="margin-top: -20px;">
                                                    <label class="col-xl-4 col-lg-4 col-form-label">Layanan </label>
                                                    <div class="col-lg-8 col-xl-8">
                                                        <input class="form-control" style="border-color:#32a852; color: #32a852" type="text" value="{{$perihal_data->layanan}}" disabled>
                                                    </div>
                                                </div>
                                                <div class="form-group row" style="margin-top: -20px;">
                                                    <label class="col-xl-4 col-lg-4 col-form-label">Perihal </label>
                                                    <div class="col-lg-8 col-xl-8">
                                                        <input class="form-control" style="border-color:#32a852; color: #32a852" type="text" value="{{$antrian_data->perihal}}" disabled>
                                                    </div>
                                                </div>
                                                <div class="form-group row" style="margin-top: -20px;">
                                                    <label class="col-xl-4 col-lg-4 col-form-label">Tanggal </label>
                                                    <div class="col-lg-8 col-xl-8">
                                                        <input class="form-control" style="border-color:#32a852; color: #32a852" type="text" value="{{date("d-M-Y", strtotime($antrian_data->tanggal))}}" disabled>
                                                    </div>
                                                </div>
                                                <div class="form-group row" style="margin-top: -20px;">
                                                    <label class="col-xl-4 col-lg-4 col-form-label">Nomor Antrean </label>
                                                    <div class="col-lg-8 col-xl-8">
                                                        <input class="form-control" style="border-color:#32a852; color: #32a852" type="text" value="{{$tiket_data->jml_antrian}}" disabled>
                                                    </div>
                                                </div>
                                                <div class="form-group row" style="margin-top: -20px;">
                                                    <label class="col-xl-4 col-lg-4 col-form-label">Waktu Antrean </label>
                                                    <div class="col-lg-8 col-xl-8">
                                                    <input class="form-control" style="border-color:#32a852; color: #32a852" type="text" value="{{$jam->waktu_mulai}} - {{$jam->waktu_selesai}}" disabled>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="kt-heading kt-heading--md" style="text-align: center;">
                                        <input class="btn btn-pill btn-warning" type="button" onclick="homeURL()" style="font-weight: bold;" value="HALAMAN UTAMA">
                                        </div>
                                    </div>
                                </div>
                                </div>
                            </div>
                    </div> 
            </div>
        </div>
    </div>
</div>


@endsection
<script>
    
    
</script>