@extends('page')
@extends('base')
@section('section')
<!-- begin:: Content -->

<div class="kt-content kt-grid__item kt-grid__item--fluid" style="padding: 20px; margin-top: 80px; background: #EBECF1; margin-bottom: 40px; border-radius: 0 8px 8px 8px; box-shadow: 0 2px 4px 0 rgba(0, 0, 0, .2), 0 3px 10px 0 rgba(0, 0, 0, .19);">
    <div id="flash-message"></div>
        <div class="kt-portlet">
            <div class="kt-portlet__body kt-portlet__body--fit">
                <div class="kt-grid kt-wizard-v3 kt-wizard-v3--white" id="kt_wizard_v3" data-ktwizard-state="step-first">
                    <div class="kt-grid__item">
                        <!--begin: Form Wizard Nav -->
                        <div class="kt-wizard-v3__nav">
                            <div class="kt-wizard-v3__nav-items"  style="justify-content: center; align-items: center;">
                                <a class="kt-wizard-v3__nav-item" data-ktwizard-type="step" data-ktwizard-state="current">
                                    <div class="kt-wizard-v3__nav-body">
                                        <div class="kt-wizard-v3__nav-label">
                                            <span>1</span> Identitas
                                        </div>
                                        <div class="kt-wizard-v3__nav-bar" style="pointer-events: none"></div>
                                    </div>
                                </a>
                                <a class="kt-wizard-v3__nav-item" data-ktwizard-type="step">
                                    <div class="kt-wizard-v3__nav-body">
                                        <div class="kt-wizard-v3__nav-label">
                                            <span>2</span> Penilaian Kesehatan
                                        </div>
                                        <div class="kt-wizard-v3__nav-bar" style="pointer-events: none"></div>
                                    </div>
                                </a>
                                <a class="kt-wizard-v3__nav-item" data-ktwizard-type="step">
                                    <div class="kt-wizard-v3__nav-body">
                                        <div class="kt-wizard-v3__nav-label">
                                            <span>3</span> Layanan & Waktu
                                        </div>
                                        <div class="kt-wizard-v3__nav-bar" style="pointer-events: none"></div>
                                    </div>
                                </a>
                                <a class="kt-wizard-v3__nav-item" data-ktwizard-type="step">
                                    <div class="kt-wizard-v3__nav-body">
                                        <div class="kt-wizard-v3__nav-label">
                                            <span>4</span> Booking
                                        </div>
                                        <div class="kt-wizard-v3__nav-bar" style="pointer-events: none"></div>
                                    </div>
                                </a>
                            </div>
                        </div>
                         <!--end: Form Wizard Nav -->
                    </div>
    
                    <!--begin: Modal -->
                    <div class="modal fade" id="kt_modal_1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Antrean Expired</h5>
                                    <button type="button" class="close" data-dismiss="modal" onclick="timeout()" aria-label="Close"> 
                                    </button>
                                </div>
                                <div class="modal-body">
                                   <p>Maaf session untuk nomor antrean yang anda pilih telah habis.</p>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary"  onclick="timeout()" data-dismiss="modal">Close</button>
                                </div> 
                            </div>
                        </div>
                    </div>

                    <div class="modal fade" id="kt_modal_npwp_empty" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Layanan ini mensyaratkan pengisian NPWP!</h5>
                                    <button type="button" class="close" data-dismiss="modal" onclick="removeModal()" aria-label="Close">
                                    </button>
                                </div>
                                <div class="modal-body">
                                   <p>Silahkan isi NPWP terlebih dahulu.</p>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary"  onclick="removeModal()" data-dismiss="modal">Close</button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="modal fade" id="modalNikTidakExist" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Kunjungan tidak diperkenankan!</h5>
                                    <button type="button" class="close" data-dismiss="modal" onclick="removeModalWaktuKunjung()" aria-label="Close">
                                    </button>
                                </div>
                                <div class="modal-body">
                                   <p>Anda sudah memiliki nomor tiket untuk jenis layanan ini.</p>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary"  onclick="removeModalWaktuKunjung()" data-dismiss="modal">Close</button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="modal fade" id="modalWaktuExist" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Kunjungan tidak diperkenankan!</h5>
                                    <button type="button" class="close" data-dismiss="modal" onclick="removeModalWaktuKunjung()" aria-label="Close">
                                    </button>
                                </div>
                                <div class="modal-body">
                                   <p>Anda sudah memiliki nomor tiket pada waktu kunjungan yang Anda pilih.</p>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary"  onclick="removeModalWaktuKunjung()" data-dismiss="modal">Close</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="modal fade" id="kt_modal_quota_kpp_doesnt_exist" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Kuota Tidak Tersedia!</h5>
                                    <button type="button" class="close" data-dismiss="modal" onclick="removeModal()" aria-label="Close">
                                    </button>
                                </div>
                                <div class="modal-body">
                                   <p>Mohon maaf layanan ini belum siap digunakan pada Kantor yang Anda pilih.</p>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary"  onclick="removeModal()" data-dismiss="modal">Close</button>
                                </div>
                            </div>
                        </div>
                    </div>
    
                    <div class="modal fade" id="kt_modal_2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Kunjungan tidak diperkenankan!</h5>
                                    <button type="button" class="close" data-dismiss="modal" onclick="timeout()" aria-label="Close">
                                    </button>
                                </div>
                                <div class="modal-body">
                                   <p>{{$info->notifikasi_odp}}</p>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary"  onclick="timeout()" data-dismiss="modal">Close</button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="modal fade" id="kt_modal_captcha" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="col-lg-8 col-xl-8">
                                                <div class="captchaa2">
                                                    <span>{!! captcha_img() !!}</span>
                                                    <button type="button" class="btn btn-brand btn-djp btn-elevate btn-pill kt-login__btn-primary"><i class="la la-refresh" id="refresh2"></i></button>
                                                </div> 
                                                <div class="input-group">
                                                    <input class="form-control" id="captcha2" name="captcha2" placeholder="Masukkan captcha" type="text" autocomplete="off" required="">
                                                </div>
                                                <input class="form-control" id="captcha_isi2" name="captcha_isi2" style="opacity: 0;width: 0; float: left;" autocomplete="off" required="">
                                             </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-success" id="captcha_valid" data-dismiss="modal">Ok !</button>&nbsp
                                    <button type="button" class="btn btn-secondary"  onclick="removeModalCaptcha()" data-dismiss="modal">Close</button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="modal fade" id="kt_modal_3" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Kuota habis!</h5>
                                    <button type="button" class="close" data-dismiss="modal" onclick="removeModal2()" aria-label="Close">
                                    </button>
                                </div>
                                <div class="modal-body">
                                   <p>Jadwal untuk jenis layanan ini sudah penuh.</p>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary"  onclick="removeModal2()" data-dismiss="modal">Close</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--end: Modal -->

                    <div class="kt-grid__item kt-grid__item--fluid kt-wizard-v3__wrapper" style="margin-top: -30px">
                        <!--begin: Form Wizard Form-->
                        <form class="kt-form" id="kt_form" style="margin-top: -30px">
                            <!--begin: Form Wizard Step 1-->

                            <div class="kt-wizard-v3__content border identitaswp" data-ktwizard-type="step-content" data-ktwizard-state="current" >
                                <div class="kt-portlet__head" style="background: #212c5f; justify-content: center; margin-bottom: -50px;">
                                    <div class="kt-portlet__head-label">
                                        <h4 class="kt-portlet__head-title" style="color: white;">
                                            Informasi Calon Pengunjung</h4>
                                    </div>
                                </div>

                                <div class="kt-form__section kt-form__section--first" style="padding: 20px;">                                    
                                    <div class="kt-wizard-v3__form">
                                        <!-- <div class="form-group row">
                                            <label class="col-form-label col-lg-3 col-sm-12">Tanggal Kunjungan*</label>
                                            <div class="col-lg-4 col-md-9 col-sm-12">
                                                <div class="input-group date">
                                                    <div class="input-group-append">
                                                        <span class="input-group-text">
                                                            <i class="la la-calendar-check-o"></i>
                                                        </span>
                                                    </div>
                                                    <input id="DatePicker">
                                                </div>
                                            </div>
                                        </div> -->
                                        
                                        <div class="form-group row">
                                            <div class="col-md-1">
                                                <div class="kt-checkbox-inline">
                                                    <span class="form-text text-muted"></span>
                                                    <label class="kt-checkbox kt-checkbox--bold kt-checkbox--brand">
                                                        <input type="checkbox" value="cbNIK" name="cbNik" id="cbNik" checked="true" autocomplete="off">
                                                            NIK <span></span>
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="col-md-1"></div>
                                            <div class="col-md-1">
                                                <div class="kt-checkbox-inline">
                                                    <span class="form-text text-muted"></span>
                                                    <label class="kt-checkbox kt-checkbox--bold kt-checkbox--brand">
                                                        <input type="checkbox" value="cbPaspor" name="cbPaspor" id="cbPaspor" autocomplete="off">
                                                            Paspor <span></span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row divPaspor" hidden>
                                            <label class="col-xl-4 col-lg-4 col-form-label">Paspor Pengunjung*</label>
                                            <div class="col-lg-8 col-md-8 col-sm-12">
                                                <div id="spinner" class="kt-spinner--sm kt-spinner--success kt-spinner--right kt-spinner--input input-group">
                                                    <input class="form-control" id="inputIdentitasPaspor" name="identitasPaspor" type="text" value="" aria-describedby="basic-addon1" autocomplete="off">
                                                </div>
                                                <span class="form-text text-muted" style="font-size: 11px">diisi dengan Paspor pengunjung</span>
                                            </div>
                                        </div>
                                        <div class="form-group row divNik">
                                            <label class="col-xl-4 col-lg-4 col-form-label">NIK Pengunjung*</label>
                                            <div class="col-lg-8 col-md-8 col-sm-12">
                                                <div id="spinner" class="kt-spinner--sm kt-spinner--success kt-spinner--right kt-spinner--input input-group">
                                                    <input class="form-control inputNik" id="inputIdentitasNik" name="identitasNik" type="text" value="" aria-describedby="basic-addon1" autocomplete="off">
                                                </div>
                                                <span class="form-text text-muted" style="font-size: 11px">diisi dengan NIK pengunjung</span>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-xl-4 col-lg-4 col-form-label">Nama Pengunjung*</label>
                                            <div class="col-lg-8 col-md-8 col-sm-12">
                                                <div class="input-group">
                                                    <input class="form-control" id="inputNama" name="nama" type="text" value="" autocomplete="off">
                                                </div>
                                                <span class="form-text text-muted" style="font-size: 11px">diisi dengan nama pengunjung</span>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-xl-4 col-lg-4 col-form-label">Status Pengunjung*</label>
                                            <div class="col-lg-8 col-md-8 col-sm-12">
                                                <div class="input-group">
                                                   <select name="statusWP" id="inputStatusWP" class="form-control" autocomplete="off">
                                                        <option value="" name="">-- Pilih Status WP --</option>
                                                        <option name="Diri Sendiri (untuk Orang Pribadi) atau Wakil Wajib Pajak (untuk pengurus Wajib Pajak Badan)" data-offset="-39600" value="Diri Sendiri atau Wakil Wajib Pajak">Diri Sendiri atau Wakil Wajib Pajak</option>
                                                        <option name="dengan menunjukkan Surat Kuasa Khusus" data-offset="-39600" value="Kuasa dari Wajib Pajak">Kuasa dari Wajib Pajak</option>
                                                        <option name="dengan membawa Surat Penunjukan bagi Layanan Tertentu" data-offset="-39600" value="Pihak Lainnya">Pihak Lainnya</option>
                                                    </select> 
                                                </div>
                                                <span class="form-text text-muted" style="font-size: 11px">WP Sendiri/Kuasa/Yang ditunjuk</span>
                                                <div class="kt-section__content kt-section__content--solid" id="divStatusWP" style=" border-left: 4px solid #fd397a; display: none;">
                                                    <div class="kt-section__content" style="margin-bottom: 0;">
                                                        <p id="disclaimerStatusWP" style="font-size: 11px; padding: 5px"></p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <!-- <div class="form-group row">
                                            <label class="col-form-label col-lg-3 col-sm-12">Tanggal Kunjungan*</label>
                                            <div class="col-lg-4 col-md-9 col-sm-12">
                                            <div class="input-group date">
                                                <div id="spinnerTanggalKunjung" class="kt-spinner--sm kt-spinner--success kt-spinner--right kt-spinner--input">
                                                    <div class="input-group-append">
                                                        <span class="input-group-text">
                                                            <i class="la la-calendar-check-o"></i>
                                                        </span>
                                                    </div>
                                                    <input type="text" class="form-control" placeholder="Pilih Tanggal" id="start" name="start" autocomplete="off"/>
                                                    <div id="kt_datepicker_2-error" class="error invalid-feedback" style="display: none;"></div>
                                                </div>
                                            </div>
                                            </div>
                                        </div> -->
                                        
                                        <div class="form-group row">
                                            <label class="col-xl-4 col-lg-4 col-form-label">NPWP</label>
                                            <div class="col-lg-8 col-xl-8">
                                            <div id="spinnerNPWP" class="kt-spinner--sm kt-spinner--success kt-spinner--right kt-spinner--input">
                                                <input class="form-control input-lg inputNPWP" id="inputNPWP" name="npwp" type="text" value="" autocomplete="off">
                                            </div>
                                            <span class="form-text text-muted" style="font-size: 11px">diisi dengan NPWP Wajib Pajak</span>
                                            <div id="NPWPTidakDiketahui" style="font-size: 80%; color: #fd397a; "></div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-xl-4 col-lg-4 col-form-label">Nama WP</label>
                                            <div class="col-lg-8 col-xl-8 input-group">
                                                <input class="form-control" id="inputNamaNPWP" name="namaNPWP" type="text" value="" disabled="" autocomplete="off">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-xl-4 col-lg-4 col-form-label">Email</label>
                                            <div class="col-lg-8 col-md-8 col-sm-12">
                                                <div class="input-group">
                                                    <input class="form-control" id="inputEmail" name="email" type="email" value="" autocomplete="off">
                                                </div>
                                                <span class="form-text text-muted" style="font-size: 11px">diisi dengan email pengunjung</span>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-xl-4 col-lg-4 col-form-label">No HP*</label>
                                            <div class="col-lg-8 col-md-8 col-sm-12">
                                                <div class="input-group">
                                                    <input class="form-control" minlength="10" id="inputNoHP" name="noHP" type="text" onkeypress="return isNumberKey(event)" value="" autocomplete="off">
                                                </div>
                                                <span class="form-text text-muted" style="font-size: 11px">diisi dengan no hp pengunjung minimal 10 digit</span>
                                            </div>
                                        </div>
                                        <div class="form-group row captcha" >
                                            <label  class="col-xl-4 col-lg-4 col-form-label" for="captcha"></label>
                                            <div class="col-lg-8 col-xl-8">
                                                <div class="captchaa">
                                                    <span>{!! captcha_img() !!}</span>
                                                    <button type="button" class="btn btn-brand btn-djp btn-elevate btn-pill kt-login__btn-primary"><i class="la la-refresh" id="refresh"></i></button>
                                                </div> 
                                                <div class="input-group">
                                                    <input class="form-control" id="captcha" name="captcha" placeholder="Masukkan captcha" type="text" autocomplete="off">
                                                </div>
                                                <input class="form-control" id="captcha_isi" name="captcha_isi" style="opacity: 0;width: 0; float: left;" autocomplete="off" >
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!--begin: Form Wizard Step 2-->
                            <div class="kt-wizard-v3__content border sak" data-ktwizard-type="step-content">
                                <div class="kt-portlet__head" style="background: #212c5f; justify-content: center; margin-bottom: -50px;">
                                    <div class="kt-portlet__head-label">
                                        <h4 class="kt-portlet__head-title" style="color: white;">
                                            Penilaian Kesehatan Mandiri</h4>
                                    </div>
                                </div>

                                <div class="kt-form__section kt-form__section--first">
                                    <div class="kt-wizard-v3__form" style="padding: 20px;">
                                        <label>Demi kesehatan dan keselamatan bersama, Calon Pengunjung harus JUJUR dalam menjawab pertanyaan dibawah ini.<br/> Dalam 14 hari terakhir, apakah Anda pernah mengalami hal-hal berikut:</label>
                                        <div class="form-group" style="margin-top: 10px">
                                            <label>Apakah Anda pernah keluar rumah/tempat umum (pasar, fasyankes, kerumunan orang, dan lain-lain)?</label>
                                            <div class="kt-radio-list">
                                                <label class="kt-radio kt-radio--success" name="radio5">
                                                    <input type="radio" class="q1_1" id="q1" name="q1" value="1" > Ya
                                                    <span></span>
                                                </label>
                                                <label class="kt-radio kt-radio--brand" name="radio5">
                                                    <input type="radio" class="q1_0" id="q1" name="q1" value="0"> Tidak
                                                    <span></span>
                                                </label>
                                            </div>
                                            <div id="q1-error" class="error invalid-feedback" style="display: none;"></div>
                                        </div>
    
                                        <div class="form-group">
                                            <label>Apakah Anda pernah menggunakan transportasi umum?</label>
                                            <div class="kt-radio-list">
                                                <label class="kt-radio kt-radio--success" name="radio5">
                                                    <input type="radio" class="q2_1" id="q2" name="q2" value="1" > Ya
                                                    <span></span>
                                                </label>
                                                <label class="kt-radio kt-radio--brand" name="radio5">
                                                    <input type="radio" class="q2_0" id="q2" name="q2" value="0"> Tidak
                                                    <span></span>
                                                </label>
                                            </div>
                                            <div id="q2-error" class="error invalid-feedback" style="display: none;"></div>
                                        </div>
    
                                        <div class="form-group">
                                            <label>Apakah Anda pernah melakukan perjalanan ke luar kota/internasional? (wilayah yang terjangkit/zona merah)</label>
                                            <div class="kt-radio-list">
                                                <label class="kt-radio kt-radio--success" name="radio5">
                                                    <input type="radio" class="q3_1" id="q3" name="q3" value="1" > Ya
                                                    <span></span>
                                                </label>
                                                <label class="kt-radio kt-radio--brand" name="radio5">
                                                    <input type="radio" class="q3_0" id="q3" name="q3" value="0"> Tidak
                                                    <span></span>
                                                </label>
                                            </div>
                                            <div id="q3-error" class="error invalid-feedback" style="display: none;"></div>
                                        </div>
    
                                        <div class="form-group">
                                            <label>Apakah Anda pernah mengikuti kegiatan yang melibatkan orang banyak?</label>
                                            <div class="kt-radio-list">
                                                <label class="kt-radio kt-radio--success" name="radio5">
                                                    <input type="radio" class="q4_1" id="q4" name="q4" value="1" > Ya
                                                    <span></span>
                                                </label>
                                                <label class="kt-radio kt-radio--brand" name="radio5">
                                                    <input type="radio" class="q4_0" id="q4" name="q4" value="0"> Tidak
                                                    <span></span>
                                                </label>
                                            </div>
                                            <div id="q4-error" class="error invalid-feedback" style="display: none;"></div>
                                        </div>
    
                                        <div class="form-group">
                                            <label>Apakah Anda pernah memiliki riwayat kontak erat dengan orang yang dinyatakan ODP, PDP, atau positif COVID-19 (berjabat tangan, berbicara, berada dalam satu ruangan/ satu rumah)?</label>
                                            <div class="kt-radio-list">
                                                <label class="kt-radio kt-radio--success" name="radio5">
                                                    <input type="radio" class="q5_1" id="q5" name="q5" value="5" > Ya
                                                    <span></span>
                                                </label>
                                                <label class="kt-radio kt-radio--brand" name="radio5">
                                                    <input type="radio" class="q5_0" id="q5" name="q5" value="0"> Tidak
                                                    <span></span>
                                                </label>
                                            </div>
                                            <div id="q5-error" class="error invalid-feedback" style="display: none;"></div>
                                        </div>
    
                                        <div class="form-group">
                                            <label>Apakah Anda pernah mengalami demam/batuk/pilek/sakit tenggorokan/sesak dalam 14 hari terakhir?</label>
                                            <div class="kt-radio-list">
                                                <label class="kt-radio kt-radio--success" name="radio5">
                                                    <input type="radio" class="q6_1" id="q6" name="q6" value="5" > Ya
                                                    <span></span>
                                                </label>
                                                <label class="kt-radio kt-radio--brand" name="radio5">
                                                    <input type="radio" class="q6_0" id="q6" name="q6" value="0"> Tidak
                                                    <span></span>
                                                </label>
                                            </div>
                                            <div id="q6-error" class="error invalid-feedback" style="display: none;"></div>
                                        </div>
                                        <div class="form-group">
                                            <label><input type="checkbox" name="disclaimer" id="disclaimer" value="1" autocomplete="off"></label> Saya menyatakan bahwa data yang diisi adalah benar dan sesuai dengan kenyataan yang sebenarnya.</label>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="kt-wizard-v3__content border layanan" data-ktwizard-type="step-content" >
                                <div class="kt-portlet__head" style="background: #212c5f; justify-content: center; margin-bottom: -40px;">
                                    <div class="kt-portlet__head-label">
                                        <h4 class="kt-portlet__head-title" style="color: white;">
                                            Layanan dan Waktu</h4>
                                    </div>
                                </div>
                                <div class="kt-form__section kt-form__section--first" style="padding: 20px;">
                                    <div class="kt-wizard-v3__form">
                                        <div class="form-group row">
                                            <label class="col-xl-3 col-lg-3 col-form-label">Kantor Tujuan*</label>
                                            <div class="col-lg-9 col-xl-9">
                                                <select name="unitEselon" id="inputUnitEselon" class="form-control" autocomplete="off">
                                                    <option value="">-- Pilih Kantor Tujuan --</option>
                                                        @foreach ($uniteselon as $key => $value)
                                                            <option value="{{ $value->id }}" required> 
                                                                {{ $value->nama_unit_lengkap }} ({{ $value->nama_unit }})
                                                            </option>
                                                        @endforeach 
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-xl-3 col-lg-3 col-form-label">Layanan*</label>
                                            <div class="col-lg-9 col-xl-9">
                                                <div id="spinnerLayanan" class="kt-spinner--sm kt-spinner--success kt-spinner--right kt-spinner--input">
                                                    <select name="layanan" id="inputLayanan" class="form-control" disabled autocomplete="off">
                                                        <option value="">-- Pilih Layanan --</option>
                                                    </select>
                                                </div>
                                                <div class="kt-section__content kt-section__content--solid" id="divDetailLayanan" style="margin-top: 10; border-left: 4px solid #fd397a; display: none;">
                                                    <div class="kt-section__content" style="margin-bottom: 0;">
                                                        <p id="disclaimer_detail_layanan" style="font-size: 11px; padding: 5px"></p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row"  style="margin-bottom: 0;">
                                            <label class="col-xl-3 col-lg-3 col-form-label">Nama Kantor*</label>
                                            <div class="col-lg-9 col-xl-9 kt-spinner--v2 kt-spinner--sm kt-spinner--success kt-spinner--right kt-spinner--input" id="spinner_unit_kerja">
                                                <input type="text" name="unitKerja" id="unitKerja" class="form-control input-lg" placeholder="Nama Unit Kerja" autocomplete="off"/> 
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-xl-3 col-lg-3 col-form-label"></label>
                                                <div id="listUnitKerja" class="col-lg-9 col-xl-9">
                                                </div>
                                        </div>
                                        
                                        <div class="form-group row">
                                            <label class="col-xl-3 col-lg-3 col-form-label">Perihal*</label>
                                            <div class="col-lg-9 col-xl-9">
                                                <textarea class="form-control" id="inputPerihal" name="perihal" type="text" value=""></textarea>
                                            </div>
                                        </div>

                                        <div class="form-group row" style="margin-top: -20px;">
                                            <label class="col-form-label col-lg-3 col-sm-12">Tanggal Kunjungan*</label>
                                            <div class="col-lg-4 col-md-9 col-sm-12">
                                            <div class="input-group date">
                                                <div id="spinnerTanggalKunjung" class="kt-spinner--sm kt-spinner--success kt-spinner--right kt-spinner--input">
                                                    <!-- <div class="input-group-append">
                                                        <span class="input-group-text">
                                                            <i class="la la-calendar-check-o"></i>
                                                        </span>
                                                    </div> -->
                                                    <input type="text" class="form-control" placeholder="Pilih Tanggal" id="start" name="date" autocomplete="off" disabled/>
                                                    <div id="kt_datepicker_2-error" class="error invalid-feedback" style="display: none;"></div>
                                                </div>
                                            </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-form-label col-lg-3 col-sm-12">Waktu Kunjungan*</label>
                                            <div class="col-lg-4 col-md-9 col-sm-12">
                                                <select name="waktuKunjung" id="inputWaktuKunjung" class="form-control" disabled autocomplete="off">
                                                    <option value="">-- Pilih Waktu Kunjungan --</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--end: Form Wizard Step 1-->
    
                            <!--begin: Form Wizard Step 4-->
                            <div class="kt-wizard-v3__content border tiket" data-ktwizard-type="step-content" style="padding: 20px">
                                <div class="kt-heading kt-heading--md" style="text-align: center">Informasi Antrean</div>
                                <div class="kt-form__section kt-form__section--first">
                                    <div class="kt-wizard-v3__form">
                                        <div class="form-group row">
                                            <label class="col-xl-4 col-lg-4 col-form-label">Nama Kantor </label>
                                            <div class="col-lg-8 col-xl-8">
                                                <input class="form-control" id="p_unit_kerja" name="p_unit_kerja" type="text" value="" disabled>
                                            </div>
                                        </div>
                                        <div class="form-group row" style="margin-top: -20px;">
                                            <label class="col-xl-4 col-lg-4 col-form-label">Alamat Kantor </label>
                                            <div class="col-lg-8 col-xl-8">
                                                <textarea class="form-control" id="p_alamat_unit_kerja" name="p_alamat_unit_kerja" type="text" value="" disabled></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group row" style="margin-top: -20px;">
                                            <label class="col-xl-4 col-lg-4 col-form-label">Tanggal Kunjungan </label>
                                            <div class="col-lg-8 col-xl-8">
                                                <input class="form-control" id="p_tanggal_kunjungan" name="p_tanggal_kunjungan" type="text" value="" disabled>
                                            </div>
                                        </div>
                                        <div class="form-group row" style="margin-top: -20px;">
                                            <label class="col-xl-4 col-lg-4 col-form-label">Sesi Kunjungan </label>
                                            <div class="col-lg-8 col-xl-8">
                                                <input class="form-control" id="p_sesi_kunjungan" name="p_sesi_kunjungan" type="text" value="" disabled>
                                            </div>
                                        </div>
                                        <div class="form-group row" style="margin-top: -20px;">
                                            <label class="col-xl-4 col-lg-4 col-form-label">Perihal Kunjungan </label>
                                            <div class="col-lg-8 col-xl-8">
                                                <input class="form-control" id="p_perihal_kunjungan" name="p_perihal_kunjungan" type="text" value="" disabled>
                                            </div>
                                        </div>
                                        {{-- <div class="form-group row" style="margin-top: -20px;">
                                            <label class="col-xl-4 col-lg-4 col-form-label">Nomor Tiket </label>
                                            <div class="col-lg-8 col-xl-8">
                                                <input class="form-control" id="p_booking" name="p_booking" type="text" value="" disabled>
                                            </div>
                                        </div> --}}
                                        <div class="kt-section">
                                            <span class="kt-section__info" style="text-align: center">
                                                Informasi
                                            </span>
                                            <div class="kt-section__content kt-section__content--solid">
                                                <div id="disclaimer" class="kt-section__content">
                                                    <p id="disclaimerText1"></p>
                                                    <p id="disclaimerText2"></p>
                                                    <p id="disclaimerText3"></p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
    
                            <!--end: Form Wizard Step 4-->
    
                            <!--begin: Form Actions -->
                            <div class="kt-form__actions">
                                <div class="btn btn-secondary btn-md btn-pill btn-wide kt-font-bold kt-font-transform-u sebelum" data-ktwizard-type="action-prev">
                                    Sebelumnya
                                </div>
                                <div class="btn btn-success btn-md btn-pill btn-wide kt-font-bold kt-font-transform-u booking" data-ktwizard-type="action-submit" onclick="booking()">
                                    Booking
                                </div>
                                <div class="btn btn-brand btn-md btn-pill btn-wide kt-font-bold kt-font-transform-u berikut" id="berikutnya" data-ktwizard-type="action-next"  onclick="checkInput()">
                                    Berikutnya
                                </div>
                            </div>
                            <!--end: Form Actions -->
                        </form>
    
                        <!--end: Form Wizard Form-->
                    </div>
                </div>
            </div>
        </div>
    </div>

    <input type="text" id="inputKdKPPAdm" name="kdkppadm" hidden autocomplete="off"/>
    <input type="text" name="kpp" id="kpp" class="form-control input-lg" hidden autocomplete="off"/>
    <input type="text" name="kdkpp" id="kdkpp" class="form-control input-lg" hidden autocomplete="off"/>
    <input type="text" name="namakantor" id="namakantor" class="form-control input-lg" hidden autocomplete="off"/>
    <input type="text" name="sehat" id="sehat" value="{{$info->notifikasi_sehat}}" class="form-control input-lg" hidden/>
    <input type="text" name="normal" id="normal" value="{{$info->notifikasi_normal}}" class="form-control input-lg" hidden/>
    <input type="text" name="odp" id="odp" value="{{$info->notifikasi_odp}}" class="form-control input-lg" hidden/>
<!-- end:: Content -->
@endsection
<script>

</script>