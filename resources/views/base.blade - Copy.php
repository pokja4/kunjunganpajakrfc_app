<!DOCTYPE html>
<html lang="en">

	<!-- begin::Head -->
	<head>
		<meta charset="utf-8" />
		<title>Aplikasi Kunjungan WP</title>
		<meta name="description" content="Pengambilan Tiket Antrean secara Online">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<meta name="csrf-token" content="{{ csrf_token() }}">
		<meta http-equiv="Content-Security-Policy" content="upgrade-insecure-requests">
		<!--begin::Fonts -->
        <link href="{{ asset('assets/fonts/font.css')}}" rel="stylesheet" type="text/css" />
		<!--end::Fonts -->

		<style type="text/css">
        	.ui-datepicker td a:after
		    {
		        content: "";
		        display: block;
		        text-align: center;
		        color: Blue;
		        font-size: small;
		        font-weight: bold;
		    }
        </style>

        <link href="{{ asset('assets/jquery-ui.css')}}" rel="stylesheet" type="text/css" />

		<link href="{{ asset('assets/css/djp/pages/custom/general/login/login-3.css')}}" rel="stylesheet" type="text/css" />
		<!--begin::Page Custom Styles(used by this page) -->
		<link href="{{ asset('assets/vendors/custom/datatables/datatables.bundle.css')}}" rel="stylesheet" type="text/css" />

		<!--end::Page Custom Styles -->

		<!--begin:: Global Mandatory Vendors -->
		<link href="{{ asset('assets/vendors/general/perfect-scrollbar/css/perfect-scrollbar.css')}}" rel="stylesheet" type="text/css" />
		<!--end:: Global Mandatory Vendors -->

		<!--begin:: Wizard -->
        <link href="{{ asset('assets/css/djp/pages/custom/general/wizard/wizard-3.css') }}" rel="stylesheet" type="text/css" />

		<!--end:: Wizard -->

		<!--begin:: Global Optional Vendors -->
		<link href="{{ asset('assets/vendors/general/tether/dist/css/tether.css')}}" rel="stylesheet" type="text/css" />
		<link href="{{ asset('assets/vendors/general/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css')}}" rel="stylesheet" type="text/css" />
		<link href="{{ asset('assets/vendors/general/bootstrap-datetime-picker/css/bootstrap-datetimepicker.css')}}" rel="stylesheet" type="text/css" />
		<link href="{{ asset('assets/vendors/general/bootstrap-timepicker/css/bootstrap-timepicker.css')}}" rel="stylesheet" type="text/css" />
		<link href="{{ asset('assets/vendors/general/sweetalert2/dist/sweetalert2.css')}}" rel="stylesheet" type="text/css" />

		<!--end:: Global Optional Vendors -->

		<!--begin::Global Theme Styles(used by all pages) -->

		<link href="{{ asset('assets/vendors/custom/vendors/line-awesome/css/line-awesome.css')}}" rel="stylesheet" type="text/css" />
		<link href="{{ asset('assets/vendors/custom/vendors/flaticon/flaticon.css')}}" rel="stylesheet" type="text/css" />
		<link href="{{ asset('assets/vendors/custom/vendors/flaticon2/flaticon.css')}}" rel="stylesheet" type="text/css" />

		<link href="{{ asset('assets/css/djp/style.bundle/base.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('assets/css/djp/style.bundle/typo.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('assets/css/djp/style.bundle/component.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('assets/css/djp/style.bundle/part.min.css')}}" rel="stylesheet" type="text/css" />

        <link href="{{ asset('assets/css/custom.css')}}" rel="stylesheet" type="text/css" />


		<link rel="shortcut icon" href="{{ asset('assets/media/logos/favicon.ico')}}" />

	</head>

	<!-- end::Head -->

	<body class="kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header-mobile--fixed kt-subheader--fixed kt-subheader--enabled kt-subheader--solid kt-aside--enabled kt-aside--fixed kt-page--loading kt-header__topbar--mobile-on">
	<!-- begin::Body -->
	
		<!-- begin:: Page -->
		@yield('content') {{-- Semua file konten kita akan ada di bagian ini --}}
		<!-- end:: Page -->

		<!-- begin::Global Config(global config for global JS sciprts) -->
		<script>
			var KTAppOptions = {
				"colors": {
					"state": {
						"brand": "#5d78ff",
						"dark": "#282a3c",
						"light": "#ffffff",
						"primary": "#5867dd",
						"success": "#34bfa3",
						"info": "#36a3f7",
						"warning": "#ffb822",
						"danger": "#fd3995"
					},
					"base": {
						"label": ["#c5cbe3", "#a1a8c3", "#3d4465", "#3e4466"],
						"shape": ["#f0f3ff", "#d9dffa", "#afb4d4", "#646c9a"]
					}
				}
			};
		</script>

		<!-- end::Global Config -->

		<!-- captcha google -->

		

		<!--begin:: Global Mandatory Vendors -->
		<script src="{{ asset('assets/vendors/general/jquery/dist/jquery.js')}}" type="text/javascript"></script>

		<script src="{{ asset('assets/jquery-1.9.1.min.js')}}"></script>
		<script src="{{ asset('assets/jquery-ui.min.js')}}"></script>

		
		<!-- <script src="{{ asset('assets/vendors/general/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')}}" type="text/javascript"></script> -->
		
        <script src="{{ asset('assets/js/djp/pages/wizard/wizard-3.js') }}" type="text/javascript" ></script>

		<!--begin::Global Theme Bundle(used by all pages) -->
		<script src="{{ asset('assets/js/djp/scripts.bundle.js')}}" type="text/javascript"></script>
		
		<!--end::Global Theme Bundle -->

		<!--begin::Page Scripts(used by this page) -->
        <!-- <script src="{{ asset('assets/js/djp/pages/crud/forms/widgets/bootstrap-datepicker.js')}}" type="text/javascript"></script> -->

		<!--end::Page Scripts -->

        <script>
            var BASE_URL = {!! json_encode(url('/')) !!};

            jQuery(document).ready(function () {
            	$("#inputNPWP").mask("99.999.999.9-999.999");
				
				$(".inputNik").mask("9999999999999999");
				
				$("#cari_npwp").mask("99.999.999.9-999.999");
				
				$("#cari_nik").mask("9999999999999999");
				
				//$("#cari_nomor_tiket").mask("999-999999-9999999");
				
			});

    jQuery(document).ready(function() {   
		updateDatePickerCells();
        function updateDatePickerCells(dp) {
            /* Wait until current callstack is finished so the datepicker
               is fully rendered before attempting to modify contents */
               console.log("masuk status");
            setTimeout(function () {

            	console.log("hello");
                //Fill this with the data you want to insert (I use and AJAX request).  Key is day of month
                //NOTE* watch out for CSS special characters in the value
                
                //Select disabled days (span) for proper indexing but // apply the rule only to enabled days(a)
                $('.datepicker > .datepicker-days td > *').each(function (idx, elem) {
                	console.log("for");
                    var value = cellContents[idx + 1] || 0;

                    // dynamically create a css rule to add the contents //with the :after                         
          //             selector so we don't break the datepicker //functionality 
                    var className = 'datepicker-content-' + value;

                    console.log("value "+value);
                    console.log("classname "+className);

                    if(value == 0)
		                addCSSRule('.ui-datepicker td a.' + className + ':after {content: "\\a0";}'); //&nbsp;
		            else
		                addCSSRule('.ui-datepicker td a.' + className + ':after {content: "' + value + '";}');

                    $(this).addClass(className);
                });
            }, 0);
        }
        var dynamicCSSRules = [];
        function addCSSRule(rule) {
            if ($.inArray(rule, dynamicCSSRules) == -1) {
                $('head').append('<style>' + rule + '</style>');
                dynamicCSSRules.push(rule);
            }
        }


        function getDays() {
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            var date ="";
            $.ajax({
                url: BASE_URL+"/getDays",
                type: "POST",
                async: false,
                data: {_token: CSRF_TOKEN},
                dataType: 'json',
                success: function (result) {
					 
					var d = new Date();
					var n = d.getDay();
					var a;
					var arr = [];
                    result = result - 1;

                    if(result != -1){
    					for(a = 1; a<= result; ++a){
    						var newDate = addDays(new Date(), a);
    						arr.push(newDate.getDay());
    					}
    					
    					var newArr = arr;
                        var arrBefore = arr;
    					
    					arr.forEach(function(entry) {
    						if(entry == 6){
    							newArr.push(0);
    						}
    						else if(entry == 0){
    							newArr.push(6);
    						}
    					});
                    // console.log(arrBefore.length);
                    // console.log("arr");
    					
                        if(arr.length > 6 ){
                            console.log('heyy')
        					if (newArr[newArr.length - 1] === 0) {
        					    date = newArr.length + 1;
        					} else if (newArr[newArr.length - 1] === 6) {
        					    date = newArr.length + 2;
        					} else {
        						date = newArr.length;
        					} 
                        }else{
                            if (newArr[newArr.length - 1] === 0) {
                                date = newArr.length + 1;
                            } else {
                                date = newArr.length;
                            } 
                        }
                        date = '+'+date.toString();

                    }else{
                        date = -1;

                    }

                }
            });
            return date;
        }

        // minimum setup
        $('#DatePicker, #kt_datepicker_1_validate').datepicker({
            changeMonth: true,
		    changeYear: true,
		    minDate: 0,
		    //The calendar is recreated OnSelect for inline calendar
		    onSelect: function (date, dp) {
		        updateDatePickerCells();
		    },
		    onChangeMonthYear: function(month, year, dp) {
		        updateDatePickerCells();
		    },
		    beforeShow: function(elem, dp) { //This is for non-inline datepicker
		        updateDatePickerCells();
		    }
        });

	});
        </script>
	</body>
	<!-- end::Body -->

</html>