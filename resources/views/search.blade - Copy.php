@extends('page')
@extends('base')
@section('section')
<!-- begin:: Content -->

<div class="kt-content kt-grid__item kt-grid__item--fluid" style="padding: 20px; background: #EBECF1; margin-bottom: 40px; border-radius: 0 8px 8px 8px; box-shadow: 0 2px 4px 0 rgba(0, 0, 0, .2), 0 3px 10px 0 rgba(0, 0, 0, .19);">
<div id="flash-message"></div>
    <div class="kt-portlet">
        <div class="kt-portlet__body kt-portlet__body--fit">
            <div class="kt-grid kt-wizard-v3 kt-wizard-v3--white" id="kt_wizard_v3" data-ktwizard-state="step-first">
                
                <div class="kt-grid__item kt-grid__item--fluid kt-wizard-v3__wrapper" style="margin-top: 10px">
                    <!--begin: Form Wizard Form-->
                    <form class="kt-form" id="kt_form" style="margin-top: 10px">
                        <!--begin: Form Wizard Step 1-->
                         <div class="kt-wizard-v3__content border identitaswp" style="padding: 20px" data-ktwizard-type="step-content" data-ktwizard-state="current" >
                            <div class="kt-heading kt-heading--md" style="text-align: center">Pencarian Tiket</div>
                            <div class="kt-form__section kt-form__section--first">
                                <div class="kt-wizard-v3__form">
                                    <div class="form-group row">
                                            <label class="col-xl-3 col-lg-3 col-form-label">Nama Unit Kerja</label>
                                            <div class="col-lg-9 col-xl-9">
                                                <input type="text" name="kpp" id="kpp" class="form-control input-lg" placeholder="Nama Unit Kerja" hidden />
                                                <input type="text" name="unit_kerja" id="unit_kerja" class="form-control input-lg" placeholder="Nama Unit Kerja" />
                                                <div id="list_unit_kerja">
                                                </div>
                                            </div>
                                        </div>
                                    <div class="form-group row">
                                        <label class="col-form-label col-lg-3 col-sm-12">Tanggal Kunjungan</label>
                                        <div class="col-lg-4 col-md-9 col-sm-12">
                                            <div class="input-group date">
                                                <input type="text" class="form-control" readonly placeholder="Pilih Tanggal" id="kt_datepicker_3" name="date" required />
                                                <div class="input-group-append">
                                                    <span class="input-group-text">
                                                        <i class="la la-calendar-check-o"></i>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-xl-3 col-lg-3 col-form-label">NIK</label>
                                        <div class="col-lg-9 col-xl-9">
                                            <input class="form-control" id="cari_nik" name="nik" type="text" value="" required>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-xl-3 col-lg-3 col-form-label">NPWP</label>
                                        <div class="col-lg-9 col-xl-9">
                                            <input class="form-control" id="cari_npwp" name="npwp" type="text" value="">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-xl-3 col-lg-3 col-form-label"></label>
                                        <div class="col-lg-9 col-xl-9">
                                            <button type="submit" id="caritiket" class="btn btn-warning btn-pill btn-elevate kt-login__btn-primary" style="color: #02275d; font-weight: bold;"><i class="flaticon2-search"></i> Cari TIKET</button>&nbsp;
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <!--end: Form Wizard Step 1-->

                    </form>
                    <!--end: Form Wizard Form-->
                         
                </div>
            </div>

            <div id="notfound"></div>

                <!--begin: Modal -->
                <div class="modal fade" id="kt_modal_caritiket" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Antrian</h5>
                            </div>
                            <div class="modal-body">
                                    <div class="kt-grid kt-wizard-v3 kt-wizard-v3--white caritiket" id="kt_wizard_v3" data-ktwizard-state="step-first">
                                        <div class="kt-grid__item">
                                            <div class="kt-grid__item kt-grid__item--fluid kt-wizard-v3__wrapper">
                                                <div class="kt-wizard-v3__content col-md-12" style="padding: 5px">
                                                <div class="kt-heading kt-heading--md" style="text-align: center; font-weight: bold; margin-top:0px;">Nomor Tiket - <div id="hasilnomor"></div>
                                                <div class="kt-form__section kt-form__section--first" style="margin-top:10px">
                                                    <div class="kt-wizard-v3__form">
                                                        <div class="form-group row">
                                                            <label class="col-xl-4 col-lg-4 col-form-label" style="text-align:left">Nama</label>
                                                            <div class="col-lg-8 col-xl-8">
                                                                <input class="form-control" id="hasilnama" style="border-color:#32a852; color: #32a852" type="text" value="" disabled>
                                                            </div>
                                                        </div>
                                                        <div class="form-group row" style="margin-top: -20px;">
                                                            <label class="col-xl-4 col-lg-4 col-form-label" style="text-align:left">NIK </label>
                                                            <div class="col-lg-8 col-xl-8">
                                                                <input class="form-control" id="hasilnik" style="border-color:#32a852; color: #32a852" type="text" value="" disabled>
                                                            </div>
                                                        </div>
                                                        <div class="form-group row" style="margin-top: -20px;">
                                                            <label class="col-xl-4 col-lg-4 col-form-label" style="text-align:left">NPWP </label>
                                                            <div class="col-lg-8 col-xl-8">
                                                                <input class="form-control" id="hasilnpwp" style="border-color:#32a852; color: #32a852" type="text" value="" disabled>
                                                            </div>
                                                        </div>
                                                        <div class="form-group row" style="margin-top: -20px;">
                                                            <label class="col-xl-4 col-lg-4 col-form-label" style="text-align:left">Nama Unit Kerja </label>
                                                            <div class="col-lg-8 col-xl-8">
                                                                <input class="form-control" id="hasilnamaunitkerja" style="border-color:#32a852; color: #32a852" type="text" value="" disabled>
                                                            </div>
                                                        </div>
                                                        <div class="form-group row" style="margin-top: -20px;">
                                                            <label class="col-xl-4 col-lg-4 col-form-label" style="text-align:left">Tanggal </label>
                                                            <div class="col-lg-8 col-xl-8">
                                                                <input class="form-control" id="hasiltanggalkunjungan" style="border-color:#32a852; color: #32a852" type="text" value="" disabled>
                                                            </div>
                                                        </div>
                                                        <div class="form-group row" style="margin-top: -20px;">
                                                            <label class="col-xl-4 col-lg-4 col-form-label" style="text-align:left">Jumlah Booking </label>
                                                            <div class="col-lg-8 col-xl-8">
                                                                <input class="form-control" id="hasiltotalbooking" style="border-color:#32a852; color: #32a852" type="text" value="" disabled>
                                                            </div>
                                                        </div>
                                                        <div class="form-group row" style="margin-top: -20px;">
                                                            <label class="col-xl-4 col-lg-4 col-form-label" style="text-align:left">Antrian Ke- </label>
                                                            <div class="col-lg-8 col-xl-8">
                                                                <input class="form-control" id="hasilantrian" style="border-color:#32a852; color: #32a852" type="text" value="" disabled>
                                                            </div>
                                                        </div>
<!--                                                         <div class="kt-section">
                                                            <span class="kt-section__info" style="text-align: center">
                                                                Informasi
                                                            </span>
                                                            <div class="kt-section__content kt-section__content--solid">
                                                                <div id="disclaimer" class="kt-section__content">
                                                                    <p></p>
                                                                </div>
                                                            </div>
                                                        </div> -->
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        </div>
                                    </div>
                                </div>
                                <!--end: Form Wizard Step 1-->
                            </div>
                        </div>
                    </div>
                </div>
                <!--end: Modal -->
        </div>
    </div>
</div>

<!-- end:: Content -->
@endsection
<script>
    
    
</script>