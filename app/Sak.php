<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sak extends Model
{
    //
    protected $table = 'sak';
    protected $fillable = [
        'id_antrian', 'sak1', 'sak2', 'sak3', 'sak4', 'sak5', 'sak6', 'hasil'
    ];

    public function antrian(){
        return $this->belongsTo('App\Antrian');
    }

}
