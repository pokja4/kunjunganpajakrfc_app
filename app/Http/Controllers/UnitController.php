<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\UnitKerja;

class UnitController extends Controller
{
    public function fetch(Request $request)
    {
        if($request->ajax()) {
            if($request->kdkantor == 2){
                $jnskantor = [21, 23];
            }else if($request->kdkantor == 3){
                $jnskantor = [22, 24];
            }else{
                $jnskantor = [25];
            }

            $data = UnitKerja::where('NM_KANTOR', 'LIKE', '%'.$request->nm_unit_kerja.'%')
                ->whereIn('JNS_KANTOR', $jnskantor)
                ->take(5)
                ->get();
            $output = '';
            
            if (count($data)>0) {
                $output = '<ul class="list-group" style="display: block; position: relative; z-index: 1">';
                foreach ($data as $row){
                    $output .= '<li class="list-group-item" id="'.$row->KD_KANTOR.' '.$row->KD_KPP.'">'.$row->NM_KANTOR.'</li>';
                }
                $output .= '</ul>';
            }
            else {
                $output .= '<li class="list-group-item">'.'Tidak ada data.'.'</li>';
            }
            return $output;
        }
    }
}
