<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Tiket;

class TiketController extends Controller
{

    public function saveTicket(Request $request){
        $save = new Tiket;
        $save->kodekantor = $request->kodekantor;
        $save->tanggal = $request->tanggal;
        $save->save();
        $tiket_id = $save->id;

        return $tiket_id;
    }
}
