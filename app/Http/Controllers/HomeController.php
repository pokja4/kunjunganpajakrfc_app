<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Mail;
use Illuminate\Contracts\Encryption\DecryptException;
use App\Http\Controllers\TiketController;
use GuzzleHttp\Client;
use App\Informasi;
use App\Antrian;
use App\Tiket;
use App\Layanan;
use App\QuotaLayanan;
use App\Sak;
use App\UnitKerja;
use App\Page;
use App\LogModel;
use App\UnitEselon;
use App\Waktu;
use App\TanggalMerah;
use NoCaptcha;
use \SoapClient;
use DataTables;
use Carbon\Carbon;
use Validator;

class HomeController extends Controller
{

    public function index(Request $request)
    {
        $info = Informasi::first(); 
        $image = url('/').'/assets/landing_page/'.$info->nama;

        return view('layouts/home')->with('info',$info)->with('image',$image);
    }

    public function mail()
    {
        return view('emails/mail');
    }

    public function app(Request $request)
    {   
        $info = Informasi::first(); 
        $uniteselon = UnitEselon::where('id','2')->orWhere('id','3')->orWhere('id','4')->get();
        $waktu_kunjungan = Waktu::get();

        return view('layouts/app')->with('uniteselon',$uniteselon)->with('info',$info)->with('waktu_kunjungan',$waktu_kunjungan);   
    }

    public function refreshCaptcha()
    {
        return response()->json(['captcha'=> captcha_img()]);
    }

    public function refreshCaptcha2()
    {
        return response()->json(['captcha'=> captcha_img()]);
    }

    public function refreshCaptcha3()
    {
        return response()->json(['captcha'=> captcha_img()]);
    }

    public function captchaValidate(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'captcha' => 'required|captcha',
        ]);
        if ($validator->passes()) {
            return response()->json(['success'=>'Added new records.']);
        }

        return response()->json(['error'=>$validator->errors()->all()]);
    }

    public function captchaValidateIsi(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'captcha_isi' => 'required',
        ]);
        if ($validator->passes()) {
            return response()->json(['success'=>'Added new records.']);
        }

        return response()->json(['error'=>$validator->errors()->all()]);
    }

    public function captchaValidate2(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'captcha2' => 'required|captcha',
        ]);
        if ($validator->passes()) {
            return response()->json(['success'=>'Added new records.']);
        }

        return response()->json(['error'=>$validator->errors()->all()]);
    }

    public function captchaValidate3(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'captcha3' => 'required|captcha',
        ]);
        if ($validator->passes()) {
            return response()->json(['success'=>'Added new records.']);
        }

        return response()->json(['error'=>$validator->errors()->all()]);
    }

    public function getIdentitas(Request $request) {
        
        $ceknik = Antrian::join('sak','antrian.id','=','sak.antrian_id')
                ->where('identitas', $request->identitas)
                ->where('sak.hasil', '>=','5')
                ->where('sak.created_at',date('Y-m-d'))
                ->count();
                
        if($ceknik > 0){
            echo 1;
        } else {
            echo 0;
        }

    }

    public function getNpwp(Request $request) {
        header('Content-Type: application/json');
        // $client = new \SoapClient('http://10.245.4.1/konfirmasi/valStatusWp?wsdl');
        $client = new \SoapClient('http://10.254.214.193/konfirmasi/valStatusWp?wsdl');
        $npwp = $request->npwp;
        $params = array(
            'npwp15' => $npwp
        );
        $response = $client->validasi($params);

        echo json_encode($response);
    }

    public function getLayanan(Request $request){
        if($request->ajax()) {
            $layanan = Layanan::where('uniteselon',$request->eselon_id)->orderBy('order','asc')->orderBy('uniteselon','asc')->get();
            $listlayanan = '<option value="">-- Pilih Layanan --</option>';
            foreach ($layanan as $row){
                $listlayanan .= '<option value="'.$row->id.' '.$row->uniteselon.' '.$row->flag_npwp.'" data-toggle="tooltip" data-placement="top" name="'.$row->deskripsi.'" required>'.$row->layanan.'</option>';
            }
            return $listlayanan;
        }
    }
    
    public function getWaktuLayanan(Request $request){
        if($request->ajax()) {
            $tanggal = date('Y-m-d');

            $weekDay = date('w', strtotime($request->tanggal_kunjungan));
            if ($weekDay == 0 || $weekDay == 6){
                return 0;
            } else {
                if($request->tanggal_kunjungan != $tanggal){
                    $getwaktu = DB::select( DB::raw("select waktu.id, waktu_mulai, waktu_selesai, kuota, COALESCE(a.jumlah_antrian, 0) AS jumlah_antrian, COALESCE((kuota - COALESCE(a.jumlah_antrian, 0)),0) as sisa from quota_layanan
                    LEFT JOIN waktu on quota_layanan.waktu_id = waktu.id
                    LEFT JOIN sk_unit_kerja on quota_layanan.kodekantor = sk_unit_kerja.KD_KANTOR
                    LEFT JOIN (select antrian.layanan_id, count(*) as jumlah_antrian from antrian, quota_layanan, waktu where antrian.layanan_id = quota_layanan.id and quota_layanan.waktu_id=waktu.id and antrian.kodekantor = '".$request->kodeKantor."' and antrian.tanggal = '".$request->tanggal_kunjungan."' and quota_layanan.layanan_id = '".$request->layananId."' and antrian.`status` in(2,3,4) group by antrian.layanan_id, quota_layanan.waktu_id) a on a.layanan_id = quota_layanan.id
                    where quota_layanan.kodekantor = '".$request->kodeKantor."' and quota_layanan.layanan_id = '".$request->layananId."' group by quota_layanan.id"));
                } else {
                    if( strlen($request->jam) < 5)
                    {
                        $jam = "0".$request->jam;
                    }else{
                        $jam = $request->jam;
                    }
                    
                    $getwaktu = DB::select( DB::raw("select waktu.id, waktu_mulai, waktu_selesai, kuota, COALESCE(a.jumlah_antrian, 0) AS jumlah_antrian, COALESCE((kuota - COALESCE(a.jumlah_antrian, 0)),0) as sisa from quota_layanan
                    LEFT JOIN waktu on quota_layanan.waktu_id = waktu.id
                    LEFT JOIN sk_unit_kerja on quota_layanan.kodekantor = sk_unit_kerja.KD_KANTOR
                    LEFT JOIN (select antrian.layanan_id, count(*) as jumlah_antrian from antrian, quota_layanan, waktu where antrian.layanan_id = quota_layanan.id and quota_layanan.waktu_id=waktu.id and antrian.kodekantor = '".$request->kodeKantor."' and antrian.tanggal = '".$request->tanggal_kunjungan."' and quota_layanan.layanan_id = '".$request->layananId."' and antrian.`status` in(2,3,4) group by antrian.layanan_id, quota_layanan.waktu_id) a on a.layanan_id = quota_layanan.id
                    where quota_layanan.kodekantor = '".$request->kodeKantor."' and waktu.waktu_mulai >= '".$jam."' and quota_layanan.layanan_id = '".$request->layananId."' group by quota_layanan.id"));
                }
                
                $waktukunjungan = '<option value="">-- Pilih Waktu Kunjungan --</option>';
                foreach ($getwaktu as $row){
                    $waktukunjungan .= '<option value="'.$row->id.'" data-placement="top" required>'.$row->waktu_mulai.' - '.$row->waktu_selesai.' (sisa kuota '.$row->sisa.')</option>';
                }
                return $waktukunjungan;
                
            }
        }
    }

    public function cekKodeKppAdm(Request $request){
        if($request->kantor == 2){
            $jnskantor = [21, 23];
        }else if($request->kantor == 3){
            $jnskantor = [22, 24];
        }else{
            $jnskantor = [25];
        }

        $cekNpwpExistInKpp = UnitKerja::where('KD_KPP', $request->kodeKPPAdm)->whereIn('JNS_KANTOR', $jnskantor)->first();
        if($cekNpwpExistInKpp){
            echo json_encode($cekNpwpExistInKpp);
        } else {
            echo 0;
        }
    }

    public function saveData(Request $request){

        $status = [2, 3, 4];

        $cekBooking = Antrian::join('quota_layanan', 'antrian.layanan_id', '=', 'quota_layanan.id')
            ->where('quota_layanan.layanan_id', $request->layananId)
            ->where('quota_layanan.waktu_id', $request->waktuKunjung)
            ->where('antrian.kodekantor', $request->kodeKantor)
            ->where('tanggal', $request->tanggal)
            ->whereIn('status', $status)
            ->count();

        if($cekBooking){
            $hasilBooking = $cekBooking + 1;
        } else {
            $hasilBooking = 1;
        }

        $checkKuota = QuotaLayanan::join('sk_unit_kerja', 'sk_unit_kerja.KD_KANTOR', '=', 'quota_layanan.kodekantor')
            ->where('quota_layanan.kodekantor', $request->kodeKantor)
            ->where('quota_layanan.waktu_id', $request->waktuKunjung)
            ->where('quota_layanan.layanan_id', $request->layananId)->first();

        if($hasilBooking <= $checkKuota->kuota){

            $saveTiket = new Tiket;
            $saveTiket->kodekantor = $request->kodeKantor;
            $saveTiket->layanan_id = $request->layananId;
            $saveTiket->tanggal = $request->tanggal;
            $saveTiket->jml_antrian = $hasilBooking;
            $saveTiket->save();
            $dataTiket = $saveTiket->id;

            if($dataTiket){ 
                $saveAntrian = new Antrian;
                $saveAntrian->identitas = $request->identitas;
                $saveAntrian->tipe_identitas = $request->tipeIdentitas;
                $saveAntrian->nama = $request->nama;
                $saveAntrian->status_wp = $request->statusWP;
                $saveAntrian->npwp = $request->npwp;
                $saveAntrian->nohp = $request->noHP;
                $saveAntrian->email = $request->email;
                $saveAntrian->perihal = $request->perihal;
                $saveAntrian->layanan_id = $checkKuota->id;
                $saveAntrian->tanggal = $request->tanggal;
                $saveAntrian->kpp = $request->kpp;
                $saveAntrian->kd_kpp = $request->kdKPP;
                $saveAntrian->kodekantor = $request->kodeKantor;
                $saveAntrian->notiket = $dataTiket;
                $saveAntrian->status = 1;
                $saveAntrian->save();
                $dataAntrian = $saveAntrian->id;

                if($dataAntrian){

                    $clientIP = request()->ip();
                    $savelog = new LogModel;
                    $savelog->id_antrian = $dataAntrian;
                    $savelog->ipaddress = $clientIP;
                    $savelog->save();

                    $savesak = new Sak;
                    $savesak->antrian_id = $dataAntrian;
                    $savesak->sak1 = $request->sak1;
                    $savesak->sak2 = $request->sak2;
                    $savesak->sak3 = $request->sak3;
                    $savesak->sak4 = $request->sak4;
                    $savesak->sak5 = $request->sak5;
                    $savesak->sak6 = $request->sak6;
                    $savesak->hasil = $request->hasil;
                    $savesak->save();
                    $datasak = $savesak->id;
                    
                    if($datasak){
                        //Cache::put('antrian_id', $dataAntrian, now()->addMinutes(10));
                        //$tanggal = str_replace("-", "", $request->tanggal);
                        $tanggal = date("ymd", strtotime($request->tanggal));  
                        $antrians = Antrian::where('id',$dataAntrian)
                        ->update([
                            'tiket' => $request->kdKPP.'-'.$tanggal.'-'.$checkKuota->layanan_id.''.$checkKuota->waktu_id.''.$hasilBooking
                        ]);
                        echo $dataAntrian;
                    }else{
                        echo 0;
                    }
                }else{
                    echo 0;
                }
            }else{
                echo 0;
            }

        } else if($checkKuota->kuota == 'NULL'){
            echo 0;
        } else {
            echo 0;
        }
    }
    
    public function cekQuotaExist(Request $request){
        $cekQuota = QuotaLayanan::where('kodekantor', $request->kodekantor)
            ->where('layanan_id',$request->layanan_id)
            ->count();
        if($cekQuota > 0){
            echo 1;
        } else {
            echo 0;
        }
    }

    public function cekQuotaTimeExist(Request $request){
        $cekQuota = QuotaLayanan::where('kodekantor', $request->kodeKantor)
            ->where('waktu_id',$request->waktuKunjung)
            ->where('layanan_id',$request->layananId)
            ->count();
        if($cekQuota > 0){
            echo 1;
        } else {
            echo 0;
        }
    }

    public function cekIdentitasExist(Request $request){
        $cekBooking = Antrian::join('quota_layanan','quota_layanan.id','=','antrian.layanan_id')
            ->where('identitas', $request->identitas)
            ->where('antrian.created_at', date('Y-m-d'))
            ->where('quota_layanan.layanan_id', $request->layananId)
            ->where('antrian.kodekantor', $request->kodeKantor)
            ->where('status', 2)
            ->count();

        $cekIdentitasExistInKpp = Antrian::join('quota_layanan','quota_layanan.id','=','antrian.layanan_id')
            ->where('identitas', $request->identitas)
            ->where('antrian.created_at', date('Y-m-d'))
            ->where('quota_layanan.layanan_id', $request->layananId)
            ->where('status', 2)
            ->count();

        if($cekBooking > 0 || $cekIdentitasExistInKpp >= 2){
           echo 1;

        } else {
           echo 0;
        }
    }

    public function checkAntrianPerWaktu(Request $request){

        if($request->ajax()){
            $cekLayanan = Antrian::join('quota_layanan','quota_layanan.id','=','antrian.layanan_id')
            ->where('identitas', $request->identitas)
            ->where('antrian.tanggal', $request->tanggal)
            ->where('quota_layanan.waktu_id', $request->waktuKunjung)
            ->where('status', 2)
            ->count();
            if($cekLayanan > 0){
                echo 1;
            }else{
                echo 0;
            }
        }

    }

    public function clearCache(Request $request){

        $antrians = Antrian::where('id',$request->antrian_id)
        ->update([
            'status' => '2', //Status 2 = Cancel Booking
        ]);
        Cache::flush();
        echo 1;
        
    }

    public function checkCache(){

        $cache = Cache::get('antrian_id');
        if($cache){
            $antrian_data = Antrian::where('id', $cache)->with('sak')->first();
            return json_encode($antrian_data);
        }else{
            echo 0;
        }
    }

    public function searchTiket(Request $request){
        $uniteselon = UnitEselon::where('id','2')->orWhere('id','3')->get();
        return view('layouts/search')->with('uniteselon',$uniteselon);
    }

    public function searchTiketNik(Request $request, $nik, $tipe){
        // $nik = '32023303119300040';
        if ($request->ajax()) {
            $antrian = Antrian::select('*', 'antrian.id as antrianid')
                    ->join('quota_layanan','antrian.layanan_id','=','quota_layanan.id')
                    ->join('layanan','quota_layanan.layanan_id','=','layanan.id')
                    ->join('waktu','waktu.id','=','quota_layanan.waktu_id')
                    ->where('antrian.status',2)
                    ->where('antrian.identitas',$nik)
                    ->where('antrian.tipe_identitas',$tipe)
                    ->where('antrian.tanggal','>=',date('Y-m-d'))
                    ->get();
                    return Datatables::of($antrian)
                            ->addIndexColumn()
                            ->addColumn('jam', function($row){
                                return $row->waktu_mulai.' - '.$row->waktu_selesai;
                            })
                            ->addColumn('action', function($row){

                                $btn = '<a href="tiket?id='.Crypt::encrypt($row->antrianid).'" class="btn btn-sm btn-info btn-elevate btn-circle btn-icon btn-icon-md" title="Show"><i class="la la-eye"></i></a>';
                           
                                return $btn;
                            })
                            
                    ->rawColumns(['jam'])
                    ->rawColumns(['action'])
                    ->make(true);
        }
        // return view('layouts/search');

        
    }

    public function getTiket(Request $request){
        $gettiket = Antrian::select('*', 'antrian.id as antrianid')
                    ->join('quota_layanan','antrian.layanan_id','=','quota_layanan.id')
                    ->where('antrian.status',2)
                    ->where('antrian.tiket',$request->nomor_tiket)
                    ->first();
        // echo $gettiket->antrianid;
        if($gettiket){
            return route('tiket', ['id' => Crypt::encrypt($gettiket->antrianid)]);
        } else {
            echo 0;
        }
    }
    
    public function cekAntrian(Request $request)
    {
        if($request->ajax()) {
            $antrian = Antrian::where('kodekantor', $request->kodekantor)
            ->where('tanggal', $request->tanggal)
            ->where('status', 1)
            ->whereBetween('updated_at', [now()->subMinutes(1), now()])
            ->count();
            echo $antrian;
        }
    }

    public function cekBooking(Request $request)
    {
        if($request->ajax()) {
            $cekBooking = Antrian::select('notiket')
            ->where('id', $request->antrian_id)
            ->first();

            $tiket = Antrian::join('sk_unit_kerja','sk_unit_kerja.KD_KANTOR', '=', 'antrian.kodekantor')
                ->join('quota_layanan','antrian.layanan_id', '=', 'quota_layanan.id')
                ->join('waktu','quota_layanan.waktu_id','=','waktu.id')
                ->where('notiket', $cekBooking->notiket)
                ->get();

            return json_encode($tiket);
        }
    }

    public function bookingAntrian(Request $request)
    {
        if($request->ajax()) {
            $cache = $request->antrian_id;
            
            $status = [2, 3, 4];
            $cekBooking = Antrian::join('quota_layanan', 'antrian.layanan_id', '=', 'quota_layanan.id')
                ->where('quota_layanan.layanan_id', $request->layanan_id)
                ->where('quota_layanan.waktu_id', $request->waktu_kunjungan)
                ->where('antrian.kodekantor', $request->kodekantor)
                ->where('tanggal', $request->tanggal)
                ->whereIn('status', $status)
                ->count();

            if($cekBooking){
                $hasilBooking = $cekBooking + 1;
            } else {
                $hasilBooking = 1;
            }

            $checkKuota = QuotaLayanan::join('sk_unit_kerja', 'sk_unit_kerja.KD_KANTOR', '=', 'quota_layanan.kodekantor')
                ->where('quota_layanan.kodekantor', $request->kodekantor)
                ->where('quota_layanan.waktu_id', $request->waktu_kunjungan)
                ->where('quota_layanan.layanan_id', $request->layanan_id)->first();

            // return $checkKuota->id;

            if($hasilBooking <= $checkKuota->kuota){

                $antrianDataBefore = Antrian::where('id', $cache)->first();

                $tikets = Tiket::where('idtiket',$antrianDataBefore->notiket)
                ->update([
                    'jml_antrian' => $hasilBooking,
                ]);

                $tanggal = date("ymd", strtotime($request->tanggal));  

                $antrians = Antrian::where('id',$cache)
                ->update([
                    'status' => '2',
                    'identitas' => $request->identitas,
                    'tipe_identitas' => $request->tipeIdentitas,
                    'nama' => $request->nama,
                    'npwp' => $request->npwp,
                    'status_wp' => $request->statusWP,
                    'npwp' => $request->npwp,
                    'nohp' => $request->noHP,
                    'email' => $request->email,
                    'perihal' => $request->perihal,
                    'layanan_id' => $checkKuota->id,
                    'tanggal' => $request->tanggal,
                    'kpp' => $request->kpp,
                    'kd_kpp' => $request->kdKPP,
                    'kodekantor' => $request->kodekantor,
                    'tiket' => $request->kdKPP.'-'.$tanggal.'-'.$checkKuota->layanan_id.''.$checkKuota->waktu_id.''.$hasilBooking
                ]);

                if($antrians){
                    $antrian_data = Antrian::where('id', $cache)->first();
                    $tiket_data = Tiket::where('idtiket', $antrian_data->notiket)->first();
                    $kuota_data = QuotaLayanan::where('id', $antrian_data->layanan_id)->first();
                    $perihal_data = Layanan::where('id', $kuota_data->layanan_id)->first();
                    $waktu_data = Waktu::where('id', $kuota_data->waktu_id)->first();
                    $waktu = $waktu_data['waktu_mulai'].'-'.$waktu_data['waktu_selesai'];
                    
                    $date = strtoupper(date("d-M-Y", strtotime($antrian_data->tanggal)));
                    
                    if($antrian_data->email){
            
                        $to_name = strtoupper($antrian_data->nama);
                        $to_email = $antrian_data->email;
                        $data = array('nama'=> strtoupper($antrian_data->nama), 'nomortiket' => $antrian_data->tiket, 'npwp' => $antrian_data->npwp, 'kpp' => $antrian_data->kpp, 'layanan' => $perihal_data->layanan, 'tanggal' => $date, 'antrian' => $tiket_data->jml_antrian, 'waktu' => $waktu);

                        Mail::send('emails.mail', $data, function($message) use ($to_name, $to_email, $data) {
                            $message->to($to_email, $to_name)
                            ->subject('Informasi Kunjungan Pajak '.$data['tanggal']);
                            $message->from('kunjungan@pajak.go.id','Informasi Kunjungan Pajak');
                        }); 
                    }
                }           

                $antrian_id = $cache;
                Cache::flush();
                return route('tiket', ['id' => Crypt::encrypt($antrian_id)]);
            } else {
                return 0;
            }
        }
    }
    
    public function tiket(Request $request){
        try{
            $id = Crypt::decrypt($request->id);
            $antrian_data = Antrian::where('id', $id)->first();
            $tiket_data = Tiket::where('idtiket', $antrian_data->notiket)->first();
            $estimasi = QuotaLayanan::join('sk_unit_kerja', 'sk_unit_kerja.KD_KANTOR', '=', 'quota_layanan.kodekantor')
                    ->where('quota_layanan.id',$antrian_data->layanan_id)
                    ->first();
            $perihal_data = Layanan::where('id', $estimasi->layanan_id)->first();
            
            $jam = Waktu::where('id',$estimasi->waktu_id)->first();
            
            $url = url('/').'/tiket?id='.$request->id;
            $info = Informasi::first();
            return view('layouts/tiket')->with(compact('antrian_data','tiket_data','perihal_data','jam','url','info','estimasi'));
            //return view('layouts/tiket');
        }catch (DecryptException $e) {
            return view('layouts/app');
        }
    }

    public function getDays(Request $request)
    {
        if($request->ajax()) {
            $info = Informasi::first(); 

            return json_encode($info->days);
        }
    }

    public function getHoliday(Request $request)
    {
        // if($request->ajax()) {
            $tanggal = TanggalMerah::get(); 
            $list_tanggal = [];
            foreach ($tanggal as $key) {
                # code...
                array_push($list_tanggal, $key->tanggal);
            }

            return $list_tanggal;
        // }
    }

    public function getKuotaDatepicker(Request $request)
    {
            
        $start = $request->startDate;
        $tommorow = date('Y-m-d',strtotime($start . "+1 days")); 

                    $tanggal2 = DB::select( DB::raw("select d.date, DAY(d.date) as day, c.kuota, v.jumlah_antri, (c.kuota - IFNULL(v.jumlah_antri,0)) as sisa from 
                        (select adddate('1970-01-01',t4.i*10000 + t3.i*1000 + t2.i*100 + t1.i*10 + t0.i) date from
                         (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t0,
                         (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t1,
                         (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t2,
                         (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t3,
                         (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t4) d
                        left join (select antrian.tanggal, count(*) as jumlah_antri from antrian left join quota_layanan on antrian.layanan_id = quota_layanan.id where antrian.tanggal between '".$tommorow."' and '".$request->endDate."' and antrian.kodekantor = '".$request->kodekantor."' and quota_layanan.layanan_id= '".$request->layananId."' and antrian.status in(2,3,4) GROUP BY antrian.tanggal) v on d.date = v.tanggal
                        join (select sum(kuota) as kuota from quota_layanan where kodekantor = '".$request->kodekantor."' and layanan_id = '".$request->layananId."') c
                        where d.date between '".$tommorow."' and '".$request->endDate."'
                        group by d.date"));
               
                    if( strlen($request->jam) < 5)
                    {
                        $jam = "0".$request->jam;
                    }else{
                        $jam = $request->jam;
                    }

                    $tanggal1 = DB::select( DB::raw("select d.date, DAY(d.date) as day, c.kuota, v.jumlah_antri, (c.kuota - IFNULL(v.jumlah_antri,0)) as sisa from 
                        (select adddate('1970-01-01',t4.i*10000 + t3.i*1000 + t2.i*100 + t1.i*10 + t0.i) date from
                         (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t0,
                         (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t1,
                         (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t2,
                         (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t3,
                         (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t4) d
                        left join (select antrian.tanggal, count(*) as jumlah_antri from antrian left join quota_layanan on antrian.layanan_id = quota_layanan.id left join waktu on quota_layanan.waktu_id=waktu.id where antrian.tanggal = '".$request->startDate."' and antrian.kodekantor = '".$request->kodekantor."' and quota_layanan.layanan_id= '".$request->layananId."' and antrian.status in(2,3,4) and waktu.waktu_mulai >= '".$jam."' GROUP BY antrian.tanggal) v on d.date = v.tanggal
                        join (select sum(kuota) as kuota from quota_layanan, waktu where quota_layanan.waktu_id=waktu.id and kodekantor = '".$request->kodekantor."' and waktu.waktu_mulai >= '".$jam."' and layanan_id = '".$request->layananId."') c
                        where d.date = '".$request->startDate."'
                        group by d.date"));
                
                    // $list_tanggal = "";
                    $list_tanggal = [];

                    foreach ($tanggal1 as $key) {
                        # code...
                        // $list_tanggal .= $key->day.':'.$key->sisa.',';
                        array_push($list_tanggal, $key->sisa);
                    }
                    
                    foreach ($tanggal2 as $key) {
                        # code...
                        // $list_tanggal .= $key->day.':'.$key->sisa.',';
                        array_push($list_tanggal, $key->sisa);
                    }

                    return $list_tanggal;
            
        
    }
}
