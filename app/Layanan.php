<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Layanan extends Model
{
    //
	protected $table = 'layanan';
    protected $fillable = [
        'id', 'layanan', 'uniteselon', 'deskripsi', 'flag_npwp'
    ];

    public function antrian(){
        return $this->belongsTo('App\Antrian');
    }

}
