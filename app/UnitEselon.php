<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UnitEselon extends Model
{
    protected $table = 'unit_eselon';
    protected $fillable = [
        'id', 'nama_unit', 'nama_unit_lengkap'
    ];
}
