<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LogModel extends Model
{
    //
    protected $table = 'userlog';
    protected $fillable = [
        'id', 'id_antrian', 'ipaddress'
    ];
}
