<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QuotaLayanan extends Model
{
    //
    protected $table = 'quota_layanan';
    protected $fillable = [
        'layanan_id', 'waktu_id', 'kodekantor', 'kuota', 'nip_peg', 'nama_peg'
    ];
}
