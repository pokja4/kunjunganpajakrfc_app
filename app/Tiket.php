<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tiket extends Model
{
    protected $table = 'tiket';
    protected $fillable = [
        'kodekantor', 'layanan_id', 'tanggal', 'jml_antrian'
    ];
}
