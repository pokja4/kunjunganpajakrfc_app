<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Perihal extends Model
{
    //
	protected $table = 'perihal';
    protected $fillable = [
        'id', 'layanan'
    ];

    public function antrian(){
        return $this->belongsTo('App\Antrian');
    }

}
