<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TanggalMerah extends Model
{
    //
    protected $table = 'tanggal_merah';
    protected $fillable = [
        'id_tanggal', 'tanggal', 'keterangan_libur', 'flag', 'kodekantor'
    ];
}
