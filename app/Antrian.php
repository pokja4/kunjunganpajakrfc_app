<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Antrian extends Model
{
    protected $table = 'antrian';
    protected $fillable = [
        'nik', 'nama', 'status_wp', 'npwp', 'nohp', 'email', 'perihal', 'kpp', 'kodekantor', 'tanggal', 'notiket', 'layanan_id', 'status'
    ];

    public function sak(){
        return $this->hasMany('App\Sak');
    }

    
}
